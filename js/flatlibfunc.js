/*-----------------------------------------------------------*
 * @ From the flat lib view, build a list of titles.
 * If the screen gor refreshed, we need to rebuild it, because the
 * array is a list of thumbnail, no title name associated with it.
 * UI: Flat Lib View 
 * Return: array of issueObject
 *-----------------------------------------------------------*/
function buildTitleList(){
	if (app_version == "howitzer") {
		var libWindow = UIATarget.localTarget().frontMostApp().windows()[0];
		var images = libWindow.scrollViews()[libScrollViewPosition].scrollViews()[0].images();
	} else {
		var libWindow = window;
		var images = libWindow.scrollViews()[libScrollViewPosition].images();
	}
	var issues = new Array();
	var totalCnt = 0;
	var imagelength = images.length;
	UIALogger.logMessage("imagelength is "+imagelength);
	for ( var i=0; i<images.length; i++){
		var imagename = images[i].name();
		//UIALogger.logMessage("imagename is "+imagename)
		if (imagename == "bg-single.png"){
			var newIssue = new issueObject (images[i].rect());
			issues[totalCnt]=newIssue;
			totalCnt++;
		}
	}
	UIALogger.logMessage ("TotalIssueCreated "+totalCnt);

	// Now provide more info to the issues
	var selectIndex = 0; 
	var currentIssue = issues[selectIndex];

	if (app_version == "howitzer") {
		var elements = libWindow.scrollViews()[libScrollViewPosition].scrollViews()[0].elements();
	} else {
		var elements = libWindow.scrollViews()[libScrollViewPosition].elements();
	}

	var reached_first_progress_halted=0;
	//UIALogger.logMessage ("TotalElement "+ elements.length);

	for ( var i=0; i<elements.length; i++){
		var elementname = elements[i].name();
		var elementvalue = elements[i].value();
		var rectInfo = elements[i].rect();
		
		if (elementname == "Progress halted") {
			reached_first_progress_halted=1;		
		}
		if (partOfIssue (currentIssue, rectInfo) == 0 && reached_first_progress_halted == 1) {
			//if (elementname != "Add More Titles" && elementname != "PickerButton_Portrait") {
				//move on to the next issue
				selectIndex++;
		}
	
		if (selectIndex < totalCnt) {
			currentIssue = issues[selectIndex];
			addMoreInfoToIssue  (currentIssue, elementname, elementvalue, rectInfo);
		} else{
			UIALogger.logMessage ("no more titleStop at element "+i);	
			break;
		}
	}
	return issues;
}

/*-----------------------------------------------------------*
 * @ Select an issue which is not pin nor downloaded and pin it
 * issues : Array of issueObject
 * UI: Lib View 
 * Return: the index of the pinning issue
 *-----------------------------------------------------------*/
function pinNotDownloadedIssue(issuesIn){
	// Now, select one has no pin and pin it
	UIALogger.logMessage("in pinNotDownloaded Issue");
	for (var i=0; i<issuesIn.length; i++){
		//UIALogger.logMessage ("check issue "+issuesIn[i].pushPin +","+issuesIn[i].download+","+issuesIn[i].pinRectX+","+issuesIn[i].pinRectY);	
		if (issuesIn[i].pushPin == 0 && issuesIn[i].download == 0){
			UIALogger.logMessage ("Select Issue "+issuesIn[i]);
			tapPin (issuesIn[i]);
			return i;
		}	
	}
	return -1;
}	
function bDLNotDownloadedIssue(issuesIn){
	// Now, select one has no pin and pin it
	UIALogger.logMessage("in bDLNotDownloaded Issue");
	UIALogger.logMessage("len is "+issuesIn.length);
	UIALogger.logMessage("bDL is "+issuesIn[0].bDL);
	for (var i=0; i<issuesIn.length; i++){
		//if (issuesIn[i].bDL == 0 && issuesIn[i].download == 0){
		if (issuesIn[i].bDL == 0) {
			UIALogger.logMessage ("Select Issue "+issuesIn[i]);
			tapbDL (issuesIn[i]);
			return i;
		}	
	}
	return -1;
}	

/*-----------------------------------------------------------*
 * @ Tap the pin button for the selected issue
 * We use the issue x.y coordinates to match the title
 * So if the lib refres, this will failed
 * issues : Array of issueObject
 * UI: Lib View 
 * Return: 1:succeed; 0:failed
 *-----------------------------------------------------------*/
function tapPin (pinThisIssue) {
	UIALogger.logMessage ("targetIssue "+ pinThisIssue);
	var btns = window.scrollViews()[libScrollViewPosition].buttons();
	var totalCnt = 0;
	for ( var i=0; i<btns.length; i++) {
		var btnname = btns[i].name();
		var rectInfo = btns[i].rect();
		if (btnname == "pinning off up") {
			if (rectInfo.origin.x == pinThisIssue.pinRectX && rectInfo.origin.y == pinThisIssue.pinRectY) {
				// This is the one we want
				UIALogger.logMessage ("Tap Issue "+btns[i]);
				btns[i].tap();
				return 1;
			}
		}	
	}
 	return 0;
}

function tapbDL (bDLThisIssue) {
	UIALogger.logMessage ("targetIssue "+ bDLThisIssue);
	var btns = window.scrollViews()[libScrollViewPosition].scrollViews()[0].buttons();
	var totalCnt = 0;
	for ( var i=0; i<btns.length; i++) {
		var btnname = btns[i].name();
		var rectInfo = btns[i].rect();
		if (btnname == "StartDL up") {
			if (rectInfo.origin.x == bDLThisIssue.bDLRectX && rectInfo.origin.y == bDLThisIssue.bDLRectY) {
				// This is the one we want
				UIALogger.logMessage ("Tap Issue "+btns[i]);
				btns[i].tap();
				return 1;
			}
		}	
	}
 	return 0;
}

/*-----------------------------------------------------------*
 * @ UnTap the pin button for the selected issue
 * We use the issue x.y coordinates to match the title
 * So if the lib refres, this will failed
 * issues : Array of issueObject
 * UI: Lib View 
 * Return: 1:succeed; 0:failed
 *-----------------------------------------------------------*/
function unPin (pinThisIssue){
	UIALogger.logMessage ("targetIssue "+ pinThisIssue);
	var btns = window.scrollViews()[libScrollViewPosition].buttons();
	var totalCnt = 0;
	for ( var i=0; i<btns.length; i++){
		var btnname = btns[i].name();
		var rectInfo = btns[i].rect();
		if (btnname == "pinning on up"){
			if (rectInfo.origin.x == pinThisIssue.pinRectX && rectInfo.origin.y == pinThisIssue.pinRectY){
				// This is the one we want
				UIALogger.logMessage ("Unpin Issue "+btns[i]);
				btns[i].tap();
				target.delay(1);
				return 1;
			}
		}	
	}
	return 0;
}
/*-----------------------------------------------------------*
 * @ issue Object constructor
 * The object holds the information we are interested of the thumbnail
 * rectIn : create this object based on thumbnail rect informaion
 *-----------------------------------------------------------*/
function issueObject(rectIn)
{
	this.x1 = rectIn.origin.x-1;
	this.y1 = rectIn.origin.y-3; // Do this because the "new" is out of the background rect
	this.x2 = rectIn.origin.x+rectIn.size.width+1;
	this.y2 = rectIn.origin.y+rectIn.size.height+1;
	this.neverRead = 0;
	this.pushPin = 0;
	this.download = 0;
	this.pinRectX = 0;
	this.pinRectY = 0;
	
}
/*-----------------------------------------------------------*
 * @ Check if the rect informaion passed in are within the rect range of the issue
 * The object holds the information we are interested of the thumbnail
 * rectIn : create this object based on thumbnail rect informaion
 * Return: 1:succeed; 0:failed
 *-----------------------------------------------------------*/
function partOfIssue (issue, rectIn){
	if (rectIn ) {
	    if (issue != null) {
		//UIALogger.logMessage ("issue "+issue.x1+","+issue.y1+","+issue.x2+","+issue.y2);
		//UIALogger.logMessage ("rectIn In "+rectIn.origin.x+","+rectIn.origin.y);
		if (rectIn.origin.x>issue.x1 &&  rectIn.origin.x<issue.x2 &&
			rectIn.origin.y>issue.y1 &&  rectIn.origin.y<=issue.y2){
			//UIALogger.logMessage ("Part of issue ");
			return 1;
			}
		}
		
	}
	return 0;
}

/*-----------------------------------------------------------*
 * @ Add more informaiton to the issueObject
 * currentIssue: The issue Object
 * elementname: name of the UI element
 * elementvalue: value of the UI element
 * rectIn: rect infomration of the UI element
 *-----------------------------------------------------------*/
function addMoreInfoToIssue  (currentIssue, elementname, elementvalue, rectIn){
	// Look into 
	// 1. newbanner.png
	//UIALogger.logMessage ("Add More info for "+ elementname);
	if (elementname == "newbanner.png") {
		currentIssue.neverRead = 1;
		//UIALogger.logMessage ("never read ");
		return;
	}
	// 2. pinning off/on up
	if (elementname == "pinning on up") {
		currentIssue.pushPin = 1;
		currentIssue.pinRectX = rectIn.origin.x;
		currentIssue.pinRectY = rectIn.origin.y;
		//UIALogger.logMessage ("pinned "+this.pinRectX);
		return;
	}
	if (elementname == "pinning off up") {
		currentIssue.pushPin = 0;
		currentIssue.pinRectX = rectIn.origin.x;
		currentIssue.pinRectY = rectIn.origin.y;
		return;
	}
	if (elementname == "StartDL up") {
		currentIssue.bDL = 0;
		UIALogger.logMessage("bDL is "+currentIssue.bDL);
		currentIssue.bDLRectX = rectIn.origin.x;
		currentIssue.bDLRectY = rectIn.origin.y;
		return;
	}
	// 3. Progressbar-fill.png width
	var isP = isProgressBar(elementname, rectIn);
	if (isP==1){
		currentIssue.download = checkProgressBarCompleted (rectIn);
		UIALogger.logMessage("CURR is "+currentIssue.download);
		return;
	}
	return;
}
/*-----------------------------------------------------------*
 * @ Check if the element is progress bar
 * elementName : name of the element
 * rectIn: rect of the element
 * UI: Lib View 
 * Return: 1 if this is progressBar, 0 if not
 *-----------------------------------------------------------*/
function isProgressBar (elementName, rectIn){
	//UIALogger.logMessage ("rectIn H "+rectIn.size.height);
	if (elementName == "progressbar-fill.png" ){
		//if (rectIn.size.height == 8) {
		if (rectIn.size.height == 4) {
			return 1;
		}
		return 0;
	}
	return 0;
}
/*-----------------------------------------------------------*
 * @ Check if the the progress bar has been filled
 * rectIn : the rect of the progressbar
 * UI: Lib View 
 * Return: 1 if this is completed, 0 if not, 2 is partial
 *-----------------------------------------------------------*/
function checkProgressBarCompleted (rectIn){
	var progBarCompleteWidth=216;
	myWidth=rectIn.size.width;
	//UIALogger.logMessage("my width is "+myWidth);
	if (rectIn.size.width == progBarCompleteWidth) {
		UIALogger.logMessage ("downloaded ");
		return 1;
	}else if(rectIn.size.width < 4 ){
		// Use 4 here because 4 is considered downloaded in usage... :(
		UIALogger.logMessage ("NOT download, current is "+rectIn.size.width);
		return 0;
	}else {
		UIALogger.logMessage ("downloading, current is "+rectIn.size.width+"/"+progBarCompleteWidth);
		return 2;
	}
}
/*-----------------------------------------------------------*
 * @ Tap open an issue which has not download yet 
 * issuesIn : issue Array that we are going to check 
 * UI: Lib View 
 * Return: the index of the issue selected in the array, or -1 if
 *		   can't find any issue
 *-----------------------------------------------------------*/
function openNotDownloadedIssue(issuesIn){
	// Now, select one has no pin and pin it
	for (var i=0; i<issuesIn.length; i++){
		UIALogger.logMessage ("check issue "+issuesIn[i].pushPin +","+issuesIn[i].download+","+issuesIn[i].pinRectX+","+issuesIn[i].pinRectY);	
		if (issuesIn[i].pushPin == 0 && issuesIn[i].download == 0){
			UIALogger.logMessage ("Select Issue "+issuesIn[i]);
			UIATarget.localTarget().tap({x: issuesIn[i].x1+10, y: issuesIn[i].y1+10});
			return i;
		}	 
	}
	return -1;
}	

/*-----------------------------------------------------------*
 * @ Opens the first fully downloaded issue it sees
 * UI: Lib View
*-----------------------------------------------------------*/
function openDownloadedIssue(issuesIn){
	UIALogger.logMessage("in openDownloadedIssue");
        readerTimeout=30;
        waitForReaderTime=0;
	target.delay(2);
        sortByNew(); //put this in here to trigger a fresh buildTitleList() below
        sortByDownloaded();
	target.delay(1);
        var issuesIn = buildTitleList();
        var foundDownloadedIssue = -1;
        for (i=0; i<issuesIn.length; i++) {
   //if (issuesIn[i].bDL == 0 && issuesIn[i].download == 0){

		//if (issuesIn[i].pushPin == 1 || issuesIn[i].download == 1){
		if (issuesIn[i].download == 1){
			UIALogger.logMessage ("Select Issue "+issuesIn[i]);
                        UIATarget.localTarget().tap({x: issuesIn[i].x1+10, y: issuesIn[i].y1+10});
                        foundDownloadedIssue = i;
			return i;
                }
        }

        if (foundDownloadedIssue == -1 ) {
                UIALogger.logMessage ("No issues were downloaded");
                return 0;
        }

        while (isInReader() == 0 && waitForReaderTime<=readerTimeout) {
                target.delay(1);
                waitForReaderTime++;
        }
        if (checkForOverlay() == 1) {
            tapDoneOnOverlay();
        }

        return foundDownloadedIssue;
}


function openDledIssuesInLib() {
    UIALogger.logMessage("in openDledIssuesInLib");
    readerTimeout=120;
    waitForReaderTime=0;
    sortByNew(); //put this in here to trigger a fresh buildTitleList() below
    sortByDownloaded();
    var foundDownloadedIssue=-1;
    var issuesIn = buildTitleList();

    UIALogger.logMessage("length is "+issuesIn.length);
    
    for (iIssue=0; iIssue<issuesIn.length; iIssue++) {
		UIALogger.logMessage ("check issue "+issuesIn[iIssue].pushPin +","+issuesIn[iIssue].download+","+issuesIn[iIssue].pinRectX+","+issuesIn[iIssue].pinRectY);
		if (issuesIn[iIssue].pushPin == 1 || issuesIn[iIssue].download == 1){
			UIALogger.logMessage ("Select Issue "+issuesIn[iIssue]);
                        UIATarget.localTarget().tap({x: issuesIn[iIssue].x1+10, y: issuesIn[iIssue].y1+10});
                        foundDownloadedIssue = iIssue;
               }

	        if (foundDownloadedIssue == -1 ) {
       		         UIALogger.logMessage ("No issues were downloaded");
       		         return 0;
       		 }
	
    		while (isInReader() == 0 && waitForReaderTime<=readerTimeout) {
       		  target.delay(1);
       		  waitForReaderTime++;
     		}
	
    		if (checkForOverlay() == 1) {
       		  tapDoneOnOverlay();
    		}
 	 
      		 
       		doVerifyIndexOverlay();
		UIALogger.logPass("openAllIssuesinLib - doVerifyIndexOverlay out OK");
		target.delay(3);	
	
		sortByNew();
		sortByDownloaded();
    		var issuesIn = buildTitleList();

		
       		 UIATarget.localTarget().tap({x: issuesIn[iIssue].x1+10, y: issuesIn[iIssue].y1+10});
	
    		while (isInReader() == 0 && waitForReaderTime<=readerTimeout) {
       		  target.delay(1);
       		  waitForReaderTime++;
     		 }
		doVerifyWheelMatchesIndex();
		UIALogger.logPass("openAllIssuesinLib - doVerifyWheelMatchesIndex out OK");
		target.delay(3);	
		
    		var issuesIn = buildTitleList();
	
	

    }   
    UIALogger.logMessage("openAllIssuesInLib pass!");
}


/*-----------------------------------------------------------*
 * @ Select an issue which is pinned to unpin
 * issues : Array of issueObject
 * UI: Lib View
 * Return: the index of the pinning issue
 *-----------------------------------------------------------*/
function unpinDownloadedIssue(issuesIn){
        sortByDownloaded();
        for ( var i=0; i<issuesIn.length; i++){
                UIALogger.logMessage ("check issue "+issuesIn[i].pushPin +","+issuesIn[i].download+","+issuesIn[i].pinRectX+","+issuesIn[i].pinRectY);
                if (issuesIn[i].pushPin == 1){
                        UIALogger.logMessage ("Select Issue "+i);
                        unPin (issuesIn[i]);
                        return i;
                }
        }
        return -1;
}

/*-----------------------------------------------------------*
 * @ Checks to see if "Deleting Issue..." activity indicator is present
 * UI: Lib view
*-----------------------------------------------------------*/
function checkDeleteInProg(selectedIn){
        var elements = window.scrollViews()[libScrollViewPosition].elements();
        var deleteInProg = 0;
        for (  var i=0; i<elements.length; i++){
                var elementname = elements[i].name();
                var elementvalue = elements[i].value();
                var rectInfo = elements[i].rect();
                if (elementname != null) {
                        if (elementname == "Deleting Issue...") {
                                 //UIALogger.logMessage ("Found element: "+elementname);
                                 deleteInProg = 1;
                                }
                }
        }
        return deleteInProg;
}


/*-----------------------------------------------------------*
 * @ Deletes the first downloaded issue it seese
 * UI: Lib View
 * Requirements:  1 downloaded issue
 * (Right now, no option to cancel the delete.)
 * (Also, an option to maybe not wait for the delete to finish?? for stress testing)
*-----------------------------------------------------------*/
function deleteSingleIssue(waitTimeForDelFinish,sort_setting){
	UIALogger.logStart("in deleteSingleIssue");
        sortBy(sort_setting);
	if (sort_setting == "Title" || sort_setting == "Date") {
		goLibToExploded();
	}

        var issuesIn = buildTitleList();
        var foundDownloadedIssue = -1;
        var waitCount=0;
        var maxCount=8;
        i=0;


        while (foundDownloadedIssue == -1 && i<issuesIn.length) {
                var currentIssue = issuesIn[i];
                if (currentIssue.download == 1 || currentIssue.pushPin == 1) {
                        UIALogger.logMessage ("Issue "+i+" is downloaded");
			//Made +20 for grenade, was +10 for flamethrower
                        UIATarget.localTarget().tapWithOptions({x: issuesIn[i].x1+20, y: issuesIn[i].y1+20},{duration:7});
                        foundDownloadedIssue = i;

                        //wait for the alert to go away
                        target.delay(waitTimeForDelFinish);
                        //assume if it is 0, the issue is already deleted fast or that the issue couldn't be deleted? Check alert
                        if (checkDeleteInProg() == 1 && waitTimeForDelFinish>0) {
                                while (checkDeleteInProg() == 1 && waitCount<maxCount) {
                                        //UIALogger.logMessage("in while loop. wait count is "+waitCount);
                                        target.delay(1);
                                        waitCount++;
                                }
                        }
                }
                i++;
        }
        return foundDownloadedIssue;
}

/*---------------------------------------------------------*
 * @Pins an issue, but does not wait for download to complete
 *  UI: Lib View
 *---------------------------------------------------------*/
function pinNoWait (){
        sortByNew();
        var issues = buildTitleList();
        var pinnedIndex = pinNotDownloadedIssue(issues);
        UIALogger.logMessage ("PINNED INDEX "+ pinnedIndex);
        var maxCount = 600; //for 10 minutes

        var selectedIssue = issues[pinnedIndex];

        //waitMetaBlockCompleted(selectedIssue,maxCount);
        return selectedIssue;
}

/*-----------------------------------------------------------*
 * @ Opens the nth issue on the screen
 *   Does not have to be downloaded.  
 *   Up readerTimeout as needed.
 * UI: Lib view
 *-----------------------------------------------------------*/
function openSpecificIssue(issueIndex) {
        UIALogger.logMessage("in openSpecificIssue");
        readerTimeout=90;
        waitForReaderTime=0;
        //target.delay(2);
        sortByDownloaded();
        sortByNew(); //put this in here to trigger a fresh buildTitleList() below
        //target.delay(1);
        var issuesIn = buildTitleList();
        UIALogger.logMessage ("Select Issue "+issuesIn[issueIndex]);
        UIATarget.localTarget().tap({x: issuesIn[issueIndex].x1+10, y: issuesIn[issueIndex].y1+10});

        while (isInReader() == 0 && waitForReaderTime<=readerTimeout && issueReadable == "yes") {
		UIALogger.logMessage("issue readable? "+issueReadable);
                target.delay(1);
                waitForReaderTime++;
	}
       	if (checkForOverlay() == 1) {
           	tapDoneOnOverlay();
         }
	if (waitForReaderTime<readerTimeout) {
	        tapBackToLib(); 
	}
}
