/*-----------------------------------------------------------*
 * @ test NIM Login using proper username and password 
 * UI: Not log in yet
 *-----------------------------------------------------------*/
function tcLogin1 (username, password){
	UIALogger.logStart ("tcLogin1: Test Login using proper username and password");
	if (testLogin (username, password) == 1) { 
		UIALogger.logPass ("tcLogin 1 Passed");
		return 1;
	} else {
		UIALogger.logFail ("tcLogin 1 Failed");
		return 0;
	}
}

function tcLogout (username) {
	UIALogger.logStart ("tcLogout");
	testLogout (username, password)
	//currently returns pass.  change this to see that we are really logged out later.
	return 1;
}

/*-----------------------------------------------------------*
 * @ test Login using FB credentials
 * UI: Not log in yet; iOS 6 FB pre-signed in only!
 *-----------------------------------------------------------*/
function tcFBLogin1 (FBusername, FBpassword){
	UIALogger.logStart ("tcFBLogin1: Test Login using proper FBusername and FBpassword");
	if (testLogin (FBusername, FBpassword,1) == 1) {
		UIALogger.logPass ("tcLoginFB 1 Passed");
		return 1;
	} else {
		UIALogger.logFail ("tcLoginFB 1 Failed");
		return 0;
	}
}

/*-----------------------------------------------------------*
 * @ test Disconnect Facebook
 * UI: logged in, already connected to facebook
 *-----------------------------------------------------------*/
function tcDisconnectFacebook(){
	UIALogger.logStart ("tcDisconnectFacebook:");
	if (disconnectFacebook() == 1) { 
		UIALogger.logPass ("tcDisconnectFacebook Passed");
		return 1;
	} else {
		UIALogger.logFail ("tcDisconnectFacebook Failed");
		return 0;
	}
}

/*-----------------------------------------------------------*
 * @ test Connect Facebook
 * UI: logged in, not connected to facebook
 *-----------------------------------------------------------*/
function tcConnectFacebookInApp(){
	UIALogger.logStart ("tcConnectFacebookInApp:");
	if (connectFacebookInApp() == 1) { 
		UIALogger.logPass ("tcConnectFacebookInApp Passed");
		return 1;
	} else {
		UIALogger.logFail ("tcConnectFacebookInApp Failed");
		return 0;
	}
}

/*-----------------------------------------------------------*
 * @ FB login, disconnect, connect in app, disconnect, sign out
 * UI: not logged in, not connected to facebook
 *-----------------------------------------------------------*/
function tcFBMaster (FBusername,FBpassword) {
	UIALogger.logStart ("tcFBMaster: Login, disconnect, connect, disconnect, logout");
	tcFBLogin1(FBusername,FBpassword);
	tcDisconnectFacebook();
	tcConnectFacebookInApp();
	tcDisconnectFacebook();
	tcLogout(FBusername);
	UIALogger.logPass ("tcFB Master");
}
/*-----------------------------------------------------------*
 * @ test Opt in/out the title
 * tcOptIn1Param: The title used in this test
 * This testing CAN'T judge if the title is really showing up in 
 * Libraey view, it just count the number of title changing after
 * Tap the title in the Picker view
 * UI: Lib View, either flat or stack->Lib stack view
 *-----------------------------------------------------------*/
function tcOptIn1 (tcOptIn1Param){
	UIALogger.logStart ("tcOptIn1: Test to see if an issue can be opted in using "+ tcOptIn1Param);
	testOptin (tcOptIn1Param);
	target.delay(10);
	UIALogger.logPass ("tcOptIn 1 Passed");
	return 1;
}
/*-----------------------------------------------------------*
 * @ test Opt in the title
 * tcOptIn2Param: The title used in this test, this title should
 * not be opted in the lib view yet 
 * UI: Lib View, either flat or stack->Lib stack view
 *-----------------------------------------------------------*/
function tcOptIn2 (tcOptIn2Param){
	UIALogger.logStart ("tcOptIn2: Test to see if an issue can be opted in using Change Selections Button "+ tcOptIn2Param);
	var titlecount= getTitleCount();
	changeOptin (tcOptIn2Param, titlecount);
	UIALogger.logPass ("Test Opt in a title "+ tcOptIn2Param);
	target.delay(10);
	UIALogger.logPass ("tcOptIn 2 Passed");
	return 1;
}
/*-----------------------------------------------------------*
 * @ test Notification Setting
 * toggle the notification flag from one to the other (on->off) 
 * And then back to original
 * UI: Lib View, either flat or stack->Lib stack view
 *-----------------------------------------------------------*/
function tcNotif1(){
	UIALogger.logStart ("tcNotif1: Test Notification Setting ");
	tapSetting();
	var previousflag1 = switchEmailEnable();
	var previousflag2 = switchEmailEnable();
	//The previousflag 1 and 2 should be different
	if (previousflag1 ==previousflag2){
		UIALogger.logFail ("tcNotif1: Failed Setting Notifiction" );
	}
	else {
		UIALogger.logPass ("tcNotif1: Passed Setting Notifiction" );	
	}
	target.delay(2);
	closeSettingPopup();
	return 1;
}

/*-----------------------------------------------------------*
 * @ test the Max Usage issue setting
 * Make sure the max issue setting has the 5 entries (last one is no maximum)
 * UI: Lib View, either flat or stack->Lib stack view
 *-----------------------------------------------------------*/
function tcUsage1 (){
	UIALogger.logStart ("tcUsage1: Test Usage Setting Value ");
	tapSetting();
	tapUsage();
	var size = getUsageMaxIssueNum();
	tapMaxIssues ();
	testIssueNumber (size);
	closeSettingPopup();
	UIALogger.logPass ("tcUsage1: Test Usage Setting Value ");
}

/*-----------------------------------------------------------*
 * @ test chaning max issue works 
 * Change the max issue works
 * UI: Lib View, either flat or stack->Lib stack view
 *-----------------------------------------------------------*/
function tcUsage2 (){
	UIALogger.logStart ("tcUsage2: Test Usage Setting Configuration ");
	tapSetting();
	tapUsage();
	testChangeMaxIssueNumber ();
	closeSettingPopup();
	UIALogger.logPass ("tcUsage2: Test Usage Setting Configuration ");
	return 1;
}
/*-----------------------------------------------------------*
 * @ test the pinned, unpinned, total downloaded matches the lib
 * UI: Lib View, either flat or stack->Lib stack view
 *-----------------------------------------------------------*/
function tcUsage3 (){
	UIALogger.logStart ("tcUsage3: Test Usage Number macth the lib display");
	getUsageIssueNumbers();
	sortByDownloaded();
	issuesIn = buildTitleList();
	var issuePinnedInLib = 0;
	var issueDownloadedInLib = 0;
	for ( i=0; i<issuesIn.length; i++){
		UIALogger.logMessage ("check issue "+issuesIn[i].pushPin +","+issuesIn[i].download);	
		if (issuesIn[i].pushPin == 1){
			issuePinnedInLib++;
		
		}
		if (issuesIn[i].download != 0){
			issueDownloadedInLib++;
		}
	}
	
	if (issuePinnedInLib == NIMUsagePinned){
		UIALogger.logPass ("tcUsage3: Pinned number in Usage match the current pin in lib "+ issuePinnedInLib );
	} else {
		UIALogger.logFail ("tcUsage3: Pinned number in Usage "+ NIMUsagePinned +", but lib shows "+ issuePinnedInLib);
	
	}
	if (issueDownloadedInLib == NIMUsageDownloaded){
		UIALogger.logPass ("tcUsage3: Downloaded number in Usage match the current pin in lib "+ issueDownloadedInLib );
	} else {
		UIALogger.logFail ("tcUsage3: Downloaded number in Usage "+ NIMUsageDownloaded +", but lib shows "+ issueDownloadedInLib);
	
	}
	return 1;
}

/*-----------------------------------------------------------*
 * @ test the pinned, unpinned, total downloaded updated after pin and download
 * UI: Lib View, either flat or stack->Lib stack view
 *-----------------------------------------------------------*/
function tcUsage4 (){
	getUsageIssueNumbers();
	var pinnedIssueNumberBefore = NIMUsagePinned;
	var unpinnedIssueNumberBefore = NIMUsageUnpinned;
	// Test Pin an issue
	UIALogger.logStart ("tcUsage4: Test Usage Number match the lib display after a pin and download");
	tcPin1();
	tcPD1();
	getUsageIssueNumbers();
	if ((pinnedIssueNumberBefore+1) == NIMUsagePinned){
		UIALogger.logPass ("tcUsage4: Pinned number changed from "+ pinnedIssueNumberBefore+" to "+ NIMUsagePinned);
	} else {
		UIALogger.logFail ("tcUsage4: Pinned number changed from "+ pinnedIssueNumberBefore+" to "+ NIMUsagePinned);
	
	}
	if ((unpinnedIssueNumberBefore +1) == NIMUsageUnpinned){
		UIALogger.logPass ("tcUsage4: Unpinned number changed from "+ unpinnedIssueNumberBefore +" to "+ NIMUsageUnpinned);
	} else {
		UIALogger.logFail ("tcUsage4: Unpinned number changed from "+ unpinnedIssueNumberBefore +" to "+ NIMUsageUnpinned);
	
	}
	return 1;

}

/*-----------------------------------------------------------*
 * @ Test unpin and the usage number will changed accordingly
 * UI: Lib View, either flat or stack->Lib stack view
 *-----------------------------------------------------------*/
function tcUsage5 (){
	UIALogger.logStart ("tcUsage5: Test Usage Number match the lib display after unpin");
	tcPin1();
	getUsageIssueNumbers();
	var pinnedIssueNumberBefore = NIMUsagePinned;
	var unpinnedIssueNumberBefore = NIMUsageUnpinned;
	issues = buildTitleList ();
	unpinDownloadedIssue (issues);
	getUsageIssueNumbers();
	if ((pinnedIssueNumberBefore-1) == NIMUsagePinned){
		UIALogger.logPass ("tcUsage5: Pinned number changed from "+ pinnedIssueNumberBefore+" to "+ NIMUsagePinned);
	} else {
		UIALogger.logFail ("tcUsage5: Pinned number should be "+ (pinnedIssueNumberBefore-1));
	
	}
	if ((unpinnedIssueNumberBefore +1) == NIMUsageUnpinned){
		UIALogger.logPass ("tcUsage5: Unpinned number changed from "+ unpinnedIssueNumberBefore +" to "+ NIMUsageUnpinned);
	} else {
		UIALogger.logFail ("tcUsage5: Unpinned number should be "+ (unpinnedIssueNumberBefore+1));
	
	}
	return 1;
}

/*-----------------------------------------------------------*
 * @ Test Opt out an title  
 * UI: Lib View, either flat or stack->Lib stack view
 *-----------------------------------------------------------*/
function tcOptOut1 (tcOptIn2Param){
	// Remove the title a new Title
	UIALogger.logStart ("tcOptOut1: Test Remove Title ");
	var titlecount= getTitleCount();
	changeOptin (tcOptIn2Param, titlecount);
	UIALogger.logPass ("tcOptOut1: Test  Remove Title ");
	return 1;
}

/*-----------------------------------------------------------*
 * @ Test Pin an issue and wait to complete
 *    Reports times and title/issue info.
 * UI: Lib View, either flat or stack->Lib stack view
 *-----------------------------------------------------------*/
function tcPin1 (){
        UIALogger.logStart ("tcPin1: Test Pin an issue ");
        sortByNew();
        var issues = buildTitleList();
        var pinnedIndex = pinNotDownloadedIssue(issues);
        UIALogger.logMessage ("PINNED INDEX "+ pinnedIndex);
        var maxCount = 600; //for 10 minutes

        var selectedIssue = issues[pinnedIndex];

        waitDownloadCompleted (selectedIssue, maxCount);

        //report back the issue tested by verifying in the reader.
	//UIATarget.localTarget().tap({x: issues[pinnedIndex].x1+10, y: issues[pinnedIndex].y1+10});
	/*
	UIATarget.localTarget().tap({x: issues[pinnedIndex].x1, y: issues[pinnedIndex].y1});
	target.delay(5);
	if (isInReader() == 1) {
        	getTitleAndIssueFromReader();
	        tapBackToLib();
	} else {
		UIALogger.logWarning("We could not enter the reader for this issue.  Content Versioned?");
	}

	*/

        UIALogger.logPass ("tcPin1: Test Pin an issue ");
        return 1;
}
function tcbDL1 (){
        UIALogger.logStart ("tcbDL1:  test bkg DL an issue");
        sortByNew();
        var issues = buildTitleList();
        var bDLIndex = bDLNotDownloadedIssue(issues);
        UIALogger.logMessage ("BDL INDEX "+ bDLIndex);
        var maxCount = 600; //for 10 minutes

        var selectedIssue = issues[bDLIndex];

        waitDownloadCompleted (selectedIssue, maxCount);

        //report back the issue tested by verifying in the reader.
	//UIATarget.localTarget().tap({x: issues[pinnedIndex].x1+10, y: issues[pinnedIndex].y1+10});
	/*
	UIATarget.localTarget().tap({x: issues[pinnedIndex].x1, y: issues[pinnedIndex].y1});
	target.delay(5);
	if (isInReader() == 1) {
        	getTitleAndIssueFromReader();
	        tapBackToLib();
	} else {
		UIALogger.logWarning("We could not enter the reader for this issue.  Content Versioned?");
	}

	*/

        UIALogger.logPass ("tcPin1: Test Pin an issue ");
        return 1;
}

/*-----------------------------------------------------------*
 * @ Test Download an issue, and wait to complete
 *    Reports times and title/issue info.
 * UI: Lib View, either flat or stack->Lib stack view
 *-----------------------------------------------------------*/
function tcPD1 (){
        UIALogger.logStart ("tcPD1: Test Progressive Download ");
        sortByNew();
        target.delay(2);
        var downloadResult = startProgressiveDownload ();

	//We are in the reader?
        if (downloadResult == 1){

		getTitleAndIssueFromReader();
                tapBackToLib ();
                sortByLastOpened();

		//600 seconds (10 minutes)
                var maxCount = 600;

                var issues = buildTitleList();
                var selectedIssue = issues[0];

		//wait for archives to complete, report time.
	        var archiveSeconds = timeIt(function() {waitIssuePartCompleted(selectedIssue,maxCount,"archives"); });

        	UIALogger.logPass ("tcPD1: Test Progressive Download ");
        } else {
		UIALogger.logFail ("tcPD1:  Test Progressive Download ");
	}
        return 1;
}

/*-----------------------------------------------------------*
* @ Tests the help overlay by cycling through the screens.  Add to this as desired
* UI: Lib View
*-----------------------------------------------------------*/
function tcHelpOverlay () {
        tapSetting();
        tapHelp();
        target.delay(2);
        checkForOverlay();

        for (iSwipe=0; iSwipe<4; iSwipe++) {
           swipeForward();
        }
        tapDoneOnOverlay();
        target.delay(2);
}

/*-----------------------------------------------------------*
 * tcVerifyIndexOverlay
 * UI : in Reader View
 * Purpose: Verifies the Index Overlay matches browse view (currently article title only)
 * Works in:  Midwest Living - Nov/Dec 2012
 *-----------------------------------------------------------*/
function tcVerifyIndexOverlay() {
	UIALogger.logMessage("in verifyIndexOverlay");
        var target = UIATarget.localTarget();
        var app = target.frontMostApp();
        var window = app.mainWindow();
	UIALogger.logMessage("in tcVerifyIndexOverlay");
	if (openDownloadedIssue() == -1) {
		UIALogger.logFail("No fully downloaded issues to open");
		return;
	}
	while (isInReader() == 0) {
		target.delay(1);
	}
	doVerifyIndexOverlay();
	UIALogger.logPass("tcVerifyIndexOverlay");

}

 /*-----------------------------------------------------------*
 * @ tcVerifyWheelMatchesIndex
 * UI : in reader view
 * Purpose:  Given an issue, go through the section wheels and match the Index Overlay selection
 * Known to Work with: Esquire October 2012 AND TIME JUSTICE ROBERTS ISSUE (July 16, 2012)
 *-----------------------------------------------------------*/
function tcVerifyWheelMatchesIndex() {
	UIALogger.logMessage("in tcVerifyWheelMatchesIndex");

        var target = UIATarget.localTarget();
        var app = target.frontMostApp();
        var window = app.mainWindow();
	if (openDownloadedIssue() == -1) {
		UIALogger.logFail("No fully downloaded issues to open");
		return;
	}
	while (isInReader() == 0) {
		target.delay(1);
	}
	doVerifyWheelMatchesIndex();
	UIALogger.logPass("tcVerifyWheelMatchesIndex PASS!");
}



 /*-----------------------------------------------------------*
 * @ tcSettings1
 * UI : in lib view
 * Purpose: Cycle through the settings options verifying the settings order on each return
 *-----------------------------------------------------------*/
function tcSettings1() {
        tapSetting();
        //Notfications front and back
        app.mainWindow().popover().tableViews()["Empty list"].cells()["Notifications"].tap();
        var notificationsWindow=app.mainWindow().popover().tableViews()["Empty list"].cells()["Email"];
        if (notificationsWindow == "[object UIAElementNil]") {
                tapSetting();
        } else {
                app.mainWindow().popover().navigationBar().leftButton().tap();
        }
	target.delay(1);
        verifySettingsOptions();


        //Usage front and back
        tapUsage();
	target.delay(2);
        app.mainWindow().popover().navigationBar().leftButton().tap();
	target.delay(2);
        verifySettingsOptions();


        //Help front and back - commented out for arrow smoke test
	
        app.mainWindow().popover().tableViews()["Empty list"].cells()["Help"].tap();
        tapDoneOnOverlay();
	target.delay(2);
        tapSetting();
	target.delay(2);
        verifySettingsOptions();
	

        //Legal front and back (have to tap settings again since came from help overlay)
        app.mainWindow().popover().tableViews()["Empty list"].cells()["Legal Notices"].tap();

        var legalOverlayWindow=app.navigationBar().buttons()["Done"];
        if (legalOverlayWindow == "[object UIAElementNil]") {
                tapSetting();
        } else {
	     var copyrightLink=window.scrollViews()[0].webViews()[0].links()["Copyright Policy"]
             if (copyrightLink != "[object UIAElementNil]") {
		  UIALogger.logPass("Found copyright link.  Looks like we are on the legal notices page correctly");
	     } else {
		  UIALogger.logWarning("Copyright link not found.  Check to see if Legal services is coming up");
	     }
             tapDoneOnOverlay();
             tapSetting();
        }
        verifySettingsOptions();
	closeSettingPopup();
	UIALogger.logPass("tcSettings 1 pass");

}
 /*-----------------------------------------------------------*
 * @ tcDelete1 - pins 1 issue; deletes 1 issue
 * UI : in lib view
 *-----------------------------------------------------------*/
function tcDelete1() {
	UIALogger.logStart("tcDelete 1");
	//we should make this slower but just in case...
	var waitTimeForDelFinish=10;
 	tcPin1();
	var sort_setting="Downloaded";
	deleteSingleIssue(waitTimeForDelFinish,sort_setting);
	UIALogger.logPass("tcDelete 1 ok");
}

 /*-----------------------------------------------------------*
 * @ tcDelete2 -
 *   Goes through the bulk delete paths.
 *   Verifies usage counts decrease on a long press delete 
 *    for both pinned and unpinned issues.
 * UI : in lib view
 *-----------------------------------------------------------*/
function tcDelete2() {
	UIALogger.logStart("tcDelete 2");
	var sort_setting="Downloaded";
	//We should make this slower but just in case
	var waitTimeForDelFinish=10;
	//Go through the no/yes pass of bulk deletes...
	//This should remove all issues from device and set counts to 0.
	deleteUnpinnedIssues("No");
	deleteUnpinnedIssues("Yes");
	deletePinnedIssues("No");
	deletePinnedIssues("Yes");

	//Verifies that after all issues deleted, nothing should be reported in usage settings.
	tapSetting();
	tapUsage();
        if (getPinnedIssueNumber() ==0 && getUnpinnedIssueNumber() == 0) {	
		UIALogger.logPass("tcDelete 2 - D/L Pinned AND Unpinned count is correct at 0 after removing unpinned issue from settings..");
	}
	else {
		UIALogger.logFail("Pinned OR Unpinned count is NOT 0 after removing unpinned issue  from settings");
	}
	closeSettingPopup();

	//Pin an issue then delete the issue, settings should be back to 0.
	tcPin1();
	deleteSingleIssue(waitTimeForDelFinish,sort_setting);
	tapSetting();
	tapUsage();
	if (getPinnedIssueNumber() == 0 && getUnpinnedIssueNumber() == 0) {
		UIALogger.logPass("Pinned AND Unpinned count is correct at 0 after deleting single pinned issue.");
	} else {
		UIALogger.logFail("Pinned OR Unpinned count  is incorrect for deleting a single pinned issue");
	}
	closeSettingPopup();


/*
	//Pin an issue, mark it as unpinned, then delete the issue, settings should be back to 0.
	//Commenting this out for now:  we cannot use tcPin1 with unpinDownloaded issue.  wrong issue shows.
        //Resolve.  Make doPinIssue() that takes in a parameter of issues object
	tcPin1();
	var issues = buildTitleList();
	unpinDownloadedIssue(issues);
	deleteSingleIssue(waitTimeForDelFinish,sort_setting);

	tapSetting();
	tapUsage();
	if (getPinnedIssueNumber() == 0 && getUnpinnedIssueNumber() == 0) {
		UIALogger.logPass("Pinned AND Unpinned count is correct after deleting single unpinned issue.");
	} else {
		UIALogger.logFail("Pinned OR Unpinned count  is incorrect for deleting a single unpinned issue");
	}
	closeSettingPopup();

*/
	UIALogger.logPass("tcDelete 2 ok");


}

/*-----------------------------------------------------------*
 * @ Opens and closes the first fully downloaded issue it sees
 * Parameter:  Count (how many times do you want to open/close)
 * UI: Lib View
 *  Similar function (not a test case in utility.js):  openWithChromeThenClose(count)
*-----------------------------------------------------------*/
function tcOpenCloseDownloadedIssue(count) {
    var timer=0;
    tcPin1();
    UIALogger.logMessage("in openCloseDownloadedIssue");
    for (j=0; j<count; j++) {
        UIALogger.logMessage("count is "+count);
        var issueIndex = openDownloadedIssue();
        if (issueIndex != -1) {
                while (isInReader() == 0 || timer<=60 ) {
                   target.delay(1);
		   timer++;
                }
		UIALogger.logMessage("j is "+j);
		UIALogger.logMessage("Will make wifi slower on for_qa_test. Then wait 5 secs");
		slowWifi();
		target.deactivateAppForDuration(60);
                tapBackToLib();
		UIALogger.logMessage("Will make wifi normal again.");
		normalWifi();
        } else {
                UIALogger.logMessage("No issues are downloaded yet");
        }
    }
}

/*------------------------------------------------------------(
 * @ Wrapper function Tests "Rate My App" functionality
 * UI:  Lib View with the "Rate My App" alert.
 * Function called from:  onAlert
 * loops from exploded to lib view to trigger the rate prompt.
*-----------------------------------------------------------*/
function tcDoRateApp() {
  doAlertException = 1;
  var maxRateLoops=100;

  for (i=0; i<maxRateLoops; i++) {
    target.delay(1);
    goLibToExploded();
    goExplodedToLib();
  }

}

/*----------------------------------------
 *Tests for checking various entitlements in lib after purchase
 *UI = Lib View. User is logged in.
 *Assumptions:
 * 1. You run this as a non-plan user with 1 entitlement to start 
 * 2. The entitlement in above step is not in the function list below
 * 3. You have php access and setup for http://wiki.nim.com/wiki/QAUserAccountUtil
 * 4. You change your var add_entitltement_script location accordingly. 
 *--------------------------------------*/
function tcCheckAutoRefresh() {
	
	UIALogger.logStart("tcCheckAutoRefresh");
	
	/*
        doCheckAutoRefresh("Title","sub","people",1);
        doCheckAutoRefresh("Date","sub","essence",1);
        doCheckAutoRefresh("New","issue","people",1);
        doCheckAutoRefresh("Last Opened","issue","entertainment weekly",1);
        doCheckAutoRefresh("Downloaded","sub","time",1);
        doCheckAutoRefresh("Date","basic","null",1);
        doCheckAutoRefresh("Title","sub","the new yorker",1);
	*/
        doCheckAutoRefresh("Title","premium","null",1);
	UIALogger.logPass("tcCheckAutoRefresh");
	
}

/*------------------------------------------------------------(
 *CORNER CASES
*-----------------------------------------------------------*/


/*-----------------------------------------------------------*
* @ checkPageMissings
* will progressively download an issue and check for missing pages
*-----------------------------------------------------------*/
function tcCheckPageMissings() {
   UIALogger.logStart("in tcCheckPageMissings");
   sortByNew();
   var maxSwipeRounds=1;
   if (startProgressiveDownload() == 1) {
	for (g=0; g<maxSwipeRounds; g++) {	
        	swipeForPageMissing("forward");
        	//swipeForPageMissing("backward");
	}
        return 1;
   } else {
        UIALogger.logMessage("Prog. D/L not successful");
        return -1;
   }
   UIALogger.logPass("tcCheckPageMissings");

}

/*-----------------------------------------------------------*
* @ checkPageMissingsWholeLib
* calls checkPageMissing for every issue in the library
*-----------------------------------------------------------*/
function tcCheckPageMissingsWholeLib() {
   UIALogger.logStart("in tcCheckPageMissingsWholeLib");
  var issuesToDownload=1;
  while (issuesToDownload==1) {
        issuesToDownload = tcCheckPageMissings();
  }
   UIALogger.logPass("tcCheckPageMissingsWholeLib");
}

/*---------------------------------------------------------*
* @ 1) Begins to pin "maxIssues" number of issues;
* @ 2) Begins to delete them; 3) toggles from lib to exploded view
* Purpose:  Stress test to simulate Crash
* UI:  Current "Real Simple" or 4 issue title to test with...
*---------------------------------------------------------*/
function tcLibStress1() {
	UIALogger.logStart("In tcLibStress1");
        var maxIssues=2
        var issueCount=0
        var waitTimeDeleteFinish=0;
        var doAlertException = 1;
        //Get a bunch of issues downloaded...
        for (issueCount=0; issueCount<maxIssues; issueCount++) {
                tcPin1();
        }

        issueCount=0
        maxIssues=2
        //Start pinning a bunch of new issues
        var selectedIssue = pinNoWait();
        while (selectedIssue != -1 && issueCount < maxIssues) {
                var selectedIssue = pinNoWait();
                issueCount++;
        }

        //Perform some deletes on the fully downloaded issues at the beginning of this function
        //Note to not wait for the delete to finish.
        //We have to sort by Title so that we can test "Hitting all titles upon delete"

        var sort_setting="Title";
        issueCount=0;
        var selectedIssue = deleteSingleIssue(waitTimeDeleteFinish,sort_setting);
        //If doAlertException==1, we will try to crash...see the function onAlert(alert) handling
        while (selectedIssue != -1 && issueCount < 2) {
                var selectedIssue = deleteSingleIssue(waitTimeDeleteFinish,sort_setting);
                issueCount++;
        }
	UIALogger.logPass("tcLibStress1");
}

/*--------------------------------*
* Pins many issues, then toggles lib/exploded view
* Part of the DB reactor sprint 11 fix
*----------------------------------*/
function tcLibStress2() {
     for (j=0; j<9; j++) {
	pinNoWait();
     }
     sortByTitle();
     var titlesIn = buildStackList();
     UIALogger.logMessage("titles In length is "+titlesIn.length);
     
     for (k=0; k<10; k++) {
     for (i=0; i<titlesIn.length; i++) {
	UIATarget.localTarget().tap({x: titlesIn[i].x1+10, y: titlesIn[i].y1+10});
	target.delay(1);
	goExplodedToLib();
     }
    
    }
} 

/*--------------------------------*
* Kicks of Bkg. D/Ls, then repeatedly logs out and back in
* DL-61-ADHOC Bug 3131
*----------------------------------*/
function tcLibStress3() {
    var maxPin=5;
    var maxLogInOut=5;
    for (i=0; i<maxPin; i++) {
	tcPinNoWait();
    }

    for (i=0; i<maxLogInOut; i++) {
    	testLogout(); 
	testLogin(username,password);
    }
}


/*-----------------------------------------------------------*
 * @ Verifies Index Overlays for first 9 issues that show up in flat view
 * UI: Lib view
 *-----------------------------------------------------------*/
function tcVerifyIndexOverlays() {
    for (var i=0; i<9; i++) {
        openSpecificIssue(i);
        doVerifyIndexOverlay();
    }
}

