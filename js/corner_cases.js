/*---------------------------------------------------------*
 * @Pins an issue, but does not wait for download to complete
 *  UI: Lib View
 *---------------------------------------------------------*/
function tcPinNoWait (){
        UIALogger.logStart ("tcPin3: Test Pin an issue no wait");
        sortByNew();
        var issues = buildTitleList();
        var pinnedIndex = pinNotDownloadedIssue(issues);
        UIALogger.logMessage ("PINNED INDEX "+ pinnedIndex);
        var maxCount = 600; //for 10 minutes

        var selectedIssue = issues[pinnedIndex];

        //waitMetaBlockCompleted(selectedIssue,maxCount);
        UIALogger.logPass ("tcPin3: Test Pin an issue no wait");
        return selectedIssue;
}

/*---------------------------------------------------------*
* @ 1) Begins to pin "maxIssues" number of issues;
* @ 2) Begins to delete them; 3) toggles from lib to exploded view
* Purpose:  Stress test to simulate Crash
* UI:  Current "Real Simple" or 4 issue title to test with...
*---------------------------------------------------------*/
function tcPinDeleteLibStress() {
        var maxIssues=2
        var issueCount=0
        var waitTimeDeleteFinish=0;
	var doAlertException = 1;
        //Get a bunch of issues downloaded...
        for (issueCount=0; issueCount<maxIssues; issueCount++) {
                tcPin1();
        }

        issueCount=0
        maxIssues=2
        //Start pinning a bunch of new issues
        var selectedIssue = tcPinNoWait();
        while (selectedIssue != -1 && issueCount < maxIssues) {
                var selectedIssue = tcPinNoWait();
                issueCount++;
        }

        //Perform some deletes on the fully downloaded issues at the beginning of this function
        //Note to not wait for the delete to finish.
        //We have to sort by Title so that we can test "Hitting all titles upon delete"

        var sort_setting="Title";
        issueCount=0;
        var selectedIssue = deleteSingleIssue(waitTimeDeleteFinish,sort_setting);
	//If doAlertException==1, we will try to crash...see the function onAlert(alert) handling
        while (selectedIssue != -1 && issueCount < 2) {
                var selectedIssue = deleteSingleIssue(waitTimeDeleteFinish,sort_setting);
                issueCount++;
        }
}

/*----------------------------------------
 *We check to see the number of thumbnails in the given sorted lib view
 *UI = Lib View. User is logged in.
 *--------------------------------------*/
function getThumbnailCount (sortByType) {
        if (sortByType == "Date") {
                sortByDate();
                var imagetomatch="bg-stack.png";
        } else if (sortByType == "Title") {
                sortByTitle();
                var imagetomatch="bg-stack.png";
        } else if (sortByType == "New") {
                sortByNew();
                var imagetomatch="bg-single.png";
        } else if (sortByType == "Last Opened") {
                sortByLastOpened();
                var imagetomatch="bg-single.png";
        } else if (sortByType == "Downloaded") {
                sortByDownloaded();
                var imagetomatch="bg-single.png";
        }
        //UIALogger.logMessage("imagetomatch is "+imagetomatch);
        var window = app.mainWindow();
        var images = window.scrollViews()[libScrollViewPosition].images();
        var totalCnt = 0;
        for ( var i=0; i<images.length; i++){
                var imagename = images[i].name();
                if (imagename == imagetomatch){
                        totalCnt++;
                }
        }
        return totalCnt;
}


/*----------------------------------------
 *We check to see if the new entitlement comes in.
 *UI = Lib View. User is logged in.
 *Assumptions:
 * 1. You start this script FIRST
 * 2. You run BuyIssue.php or AddSub.php on the same account as you are logged in as
 * 3.  This loop currently can run infinite if nothing new is bought for this account after this script gets run.
 *--------------------------------------*/
function checkAutoRefresh(sortByType) {
        target.delay(1);

        // record start time
        var startTime = new Date();

        //Get # of  Thumbnails based on given "Sort By"
        var origLibCount=getThumbnailCount(sortByType);
        UIALogger.logMessage("count is "+origLibCount);

        //Get # of Thumbnails again to make sure it hasn't changed.
        currLibCount=getThumbnailCount(sortByType);

        var count=0;
        //While the new stack has not appeared, keep checking.
        //We are waiting until the auto refresh of a new ent. comes in
        while ( origLibCount==currLibCount || count<=700) {
                target.delay(1);
                var currLibCount=getThumbnailCount(sortByType);
                count++;
        }

        if (count=700) {
                UIALogger.logFail("no entitlements were added");
                return;
        }
        // later record end time
        var endTime = new Date();

        // strip the miliseconds
        var timeDiff = endTime - startTime;
        timeDiff /= 1000;

        // get seconds
        var seconds = Math.round(timeDiff % 60);

        // remove seconds from the date
        timeDiff /= Math.round(60);

        // get minutes
        var minutes = Math.round(timeDiff % 60);

        UIALogger.logMessage("Minutes is "+minutes);
        UIALogger.logMessage("Curr count is NOW "+currLibCount);
        UIALogger.logMessage("Orig count was "+origLibCount);
}


/*----------------------------------------
 *@loopLoginLogout
 *LogsIn and LogsOut for as many times specified
 *--------------------------------------*/
function loopLoginLogout(maxNumLoops) {
    for (i=0; i< maxNumLoops; i++) {
	testLogin(username,password);
	testLogout(username);
    }
}

/*----------------------------------------
 *@loopExplodedLib
 *loops from exploded-lib-exploded
 *--------------------------------------*/
function loopExplodedLib(maxNumLoops) {
    for (i=0; i< maxNumLoops; i++) {
	goExplodedToLib();
        goLibToExploded();
    }
}

/*----------------------------------------
 *@loopLibExploded
 *loops from lib-exploded-lib
 *--------------------------------------*/
function loopLibExploded(maxNumLoops) {
    for (i=0; i< maxNumLoops; i++) {
        goLibToExploded();
	goExplodedToLib();
    }
}


function tcPin4 (){
        UIALogger.logStart ("tcPin4: Test Pin wait for metablock only ");
        sortByNew();
        var issues = buildTitleList();
        var pinnedIndex = pinNotDownloadedIssue(issues);
        //var pinnedIndex = 9;
        UIALogger.logMessage ("PINNED INDEX "+ pinnedIndex);
        var maxCount = 600; //for 10 minutes

        var selectedIssue = issues[pinnedIndex];

        waitMetaBlockCompleted (selectedIssue, maxCount);
	slowWifi();
	UIALogger.logMessage("Waiting 15 minutes before we turn back wifi on");
	target.delay(900);
	normalWifi();
        waitArchivesCompleted (selectedIssue, maxCount);
        UIALogger.logPass ("tcPin4: Test Pin an issue ");
        return 1;
}

