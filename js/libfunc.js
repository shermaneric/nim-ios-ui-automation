/*-----------------------------------------------------------*
 * @ Enter Picker View  
 * UI: Lib View -> Picker View
 *-----------------------------------------------------------*/
function enterPickerview (titlecount) {
	var pickerTextNumber=titlecount*4; 
        var window = UIATarget.localTarget().frontMostApp().mainWindow();	
	if (app_version == "flamethrower") {
		var addMoreButton = window.scrollViews()[libScrollViewPosition].staticTexts()["Add or Remove Magazines"];
	} else {
		var addMoreButton = window.scrollViews()[libScrollViewPosition].buttons()["Add or Remove Magazines"];
	}
	if (addMoreButton.isVisible()) {
		target.delay(1);
		addMoreButton.tap();
		UIALogger.logMessage ("Tap Add or Remove Magazines Button");
        } else {
		UIALogger.logError ("Picker Add or Remove Magazines Button NOT seen");
	}
	target.delay(2);
}

/*-----------------------------------------------------------*
 * @sort by Date
 * UI: Lib view
 *-----------------------------------------------------------*/
function sortByDate(){
	sortBy("Date");
}
/*-----------------------------------------------------------*
 * @sort by Title
 * UI: Lib view
 *-----------------------------------------------------------*/
function sortByTitle(){
	sortBy("Title");
}
/*-----------------------------------------------------------*
 * @sort by New
 * UI: Lib view
 *-----------------------------------------------------------*/
function sortByNew(){
	sortBy("New");
}
/*-----------------------------------------------------------*
 * @sort by Last Openned
 * UI: Lib view
 *-----------------------------------------------------------*/
function sortByLastOpened(){
	sortBy("Last Opened");
}
/*-----------------------------------------------------------*
 * @sort by Downloaded
 * UI: Lib view
 *-----------------------------------------------------------*/
function sortByDownloaded(){
	sortBy("Downloaded");
}

function checkForSortPopover() {
       if (app.mainWindow().popover().tableViews()["Empty list"] != "[object UIAElementNil]") {
		return 1;
	} else {
		return 0;
	}
}

/*-----------------------------------------------------------*
 * @sort by Date
 * UI: Lib view
 *-----------------------------------------------------------*/
function sortBy(sortByType){
	var maxSortPopCount=0;
	// Hard codee here, the "Date", "Time" is the first button
	var sortBy = app.navigationBar().buttons()[0].name();
	if (sortBy==sortByType){
		// we are good...
		return; 
	}else{
		
		
		while (checkForSortPopover() == 0 && maxSortPopCount<2) {
			app.navigationBar().buttons()[0].tap();
			target.delay(1);
			maxSortPopCount++;
		}
		
		
		app.mainWindow().popover().tableViews()["Empty list"].cells()[sortByType].tap();
		target.delay(2);
	}
}
/*-----------------------------------------------------------*
 * @ find the current title count in Lib view, make sure it is on stack view
 * UI: Lib View 
 * Return the title count in integer 
 *-----------------------------------------------------------*/
function getTitleCount () {
	sortByDate();
	var window = app.mainWindow();
	//window.logElementTree();
	var images = window.scrollViews()[libScrollViewPosition].images();
	var totalCnt = 0;
	for ( var i=0; i<images.length; i++){
		var imagename = images[i].name();
		if (imagename == "bg-stack.png"){
			totalCnt++;
		}
	}
	return totalCnt;
}

/*-----------------------------------------------------------*
 * @ Chagne the Opt in 
 * titlecount: current opt in title count
 * UI: Lib View-> Picker View-> Lib View
 * Return new title count in integer
 *-----------------------------------------------------------*/
function changeOptin (selectTitleName, titlecount){
	var firstTime = 0;
	if (titlecount ==0 ){
		firstTime = 1;
	}
	
	// enter picker view.  temp. passing the titlecount to * by 4 to get around old chg. seleciton bug
        //will take out cannonball+ 
	enterPickerview (titlecount);

	// Select the title in picker view
	tapTitleInPickerview(selectTitleName,1);
	target.delay(1);
	tapDoneInPickerview();
	target.delay(3);
	// Back to lib view, find the new count (delay if needed)
	var getnewcount = getTitleCount();
	var cycle=0;
	var maxWait=12;
	while (getnewcount == titlecount && cycle < maxWait ) {
		target.delay(1);
		getnewcount = getTitleCount();
	}
	if (cycle == maxWait) {		
		// Cant be the same, an error
		UIALogger.logFail ("Change Opt in "+ selectTitleName+" does not change the entitlement");
	}
	return getnewcount;
}
/*-----------------------------------------------------------*
 * @ test Opt in 
 * selectTitleName: The title will be selected/deselected in Picker View
 * UI: Lib View-> Picker View-> Lib View
 * Return new title count in integer
 *-----------------------------------------------------------*/
function testOptin (selectTitleName){
	UIALogger.logStart ("Start: Test Opt In on title "+selectTitleName);
	var titlecount= getTitleCount();
	var newtitlecount1 = changeOptin (selectTitleName, titlecount);
	
	var newtitlecount2 = changeOptin (selectTitleName, newtitlecount1);
	
	if (newtitlecount2 != titlecount)
	{
		// should be back to previous one 
		UIALogger.logFail ("Revert Change Opt in "+ selectTitleName+" change the entitlement");
	}else {
		UIALogger.logMessage ("Pass: Test Opt in on title "+selectTitleName);
	}
}

/*-----------------------------------------------------------*
 * @ Get the usage (pinned, unpinned) from the issue list
 * UI: Flat Lib View or Exploded View
 * Return: array of totalDownloaded, totalunpinned, totalpinned
 *-----------------------------------------------------------*/
 function getUsageFromIssueList (){
 
	UIALogger.logMessage ("Get Usage number from issueList");
	issuesIn = buildTitleList();
	var issuePinned = 0;
	var issueDownloaded = 0;
	for ( var i=0; i<issuesIn.length; i++){
		UIALogger.logMessage ("check issue "+issuesIn[i].download+"," +issuesIn[i].pushPin );	
		if (issuesIn[i].pushPin == 1){
			issuePinned++;
		
		}
		if (issuesIn[i].download != 0){
			issueDownloaded++;
		}
	}
	var issueUnpinned = issueDownloaded - issuePinned;
	return [issueDownloaded, issueUnpinned, issuePinned];
}

/*-----------------------------------------------------------*
 * @goLibToExploded
 * Note:  bg-stack.png is not unique, so right now hard code.  verified in portrait and landscape.
 * UI: Lib view.  User has at least 1 title
 *-----------------------------------------------------------*/
function goLibToExploded() {
	var titleIndex=1;
	sortByTitle();
	var titlesIn=buildStackList();
	UIATarget.localTarget().tap({x: titlesIn[titleIndex].x1+10, y: titlesIn[titleIndex].y1+10});
        target.delay(1);
}


function buildStackList() {
	if (app_version == "howitzer") {
                var libWindow = UIATarget.localTarget().frontMostApp().windows()[0];
                var images = libWindow.scrollViews()[libScrollViewPosition].scrollViews()[0].images();
        } else {
                var libWindow = window;
                var images = libWindow.scrollViews()[libScrollViewPosition].images();
        }

	sortByTitle();
        var titles = new Array();
        var images = window.scrollViews()[libScrollViewPosition].images();
        var totalCnt = 0;
        for ( var i=0; i<images.length; i++){
                var imagename = images[i].name();
                if (imagename == "bg-stack.png"){
                        var newTitle = new issueObject (images[i].rect());
                        titles[totalCnt]=newTitle;
                        totalCnt++;
                }
        }
        UIALogger.logMessage ("TotalStacksCreated "+totalCnt);
	return titles;
}


/*-----------------------------------------------------------*
 * @goExplodedToLib()
 * UI: Exploded view
 *-----------------------------------------------------------*/
function goExplodedToLib() {
        var allTitlesButton = window.navigationBar().buttons()["All Titles"];
        allTitlesButton.tap();
        target.delay(1);
}


/*------------------------------------------------------------*
 * @For each rate option, dismisses button and sets Rate My App expectation next time around.
*-----------------------------------------------------------*/
function rateApp(rateButton) {

   if (rateAlertExpected=="no") {
        UIALogger.logFail ("We did not expect the Rate My App alert to appear.");
        return 0;
   }

   if (rateButton == "Rate Next Issue") {
             UIATarget.localTarget().frontMostApp().alert().buttons()[rateButton].tap();
             UIALogger.logMessage("we went to rate next issue.  Verify the target link separately.");
             UIATarget.localTarget().deactivateAppForDuration(1);
             var rateAlertExpected="no";
             return 1;
   }
   if (rateButton == "No, Thanks") {
             UIATarget.localTarget().frontMostApp().alert().buttons()[rateButton].tap();
             var rateAlertExpected="no";
             return 1;
   }

   if (rateButton == "Remind Me Later") {
             rateStartTime = new Date();
             UIATarget.localTarget().frontMostApp().alert().buttons()[rateButton].tap();
             var rateAlertExpected="yes";
             return 1;
   }
}

/*------------------------------------------------------------*
 * @Sets the Rate App Timer
 * UI:  Lib View with the "Rate My App" alert.
 * Function called from:  onAlert
 * Sets Time, then goes and loops from exploded to lib view.
*-----------------------------------------------------------*/
function setRateAppTimer(rateStartTime) {
  UIALogger.logMessage("rateStartTime is "+rateStartTime);
  if (rateStartTime != "no") {
            var rateEndTime = new Date();
            // strip the miliseconds
            var timeDiff = rateEndTime - rateStartTime;
            timeDiff /= 1000;

            // get seconds
            var seconds = Math.round(timeDiff % 60);

            UIALogger.logMessage("Number of seconds for RateMyApp alert to show: "+seconds);
  } else {
            var rateStartTime = new Date();
  }
}

/*----------------------------------------
 *We check to see the number of thumbnails in the given sorted lib view
 *UI = Lib View. User is logged in.
 *called by tcCheckAutoRefresh
 *--------------------------------------*/
function getThumbnailCount (sortByType) {
	var totalCnt=0;
        if (target.frontMostApp().mainWindow().staticTexts()["Welcome To Next Issue"] != "[object UIAElementNil]") {
		return totalCnt;
        } else if (sortByType == "Date") {
                sortByDate();
                var imagetomatch="bg-stack.png";
        } else if (sortByType == "Title") {
                sortByTitle();
                var imagetomatch="bg-stack.png";
        } else if (sortByType == "New") {
                sortByNew();
                var imagetomatch="bg-single.png";
        } else if (sortByType == "Last Opened") {
                sortByLastOpened();
                var imagetomatch="bg-single.png";
        } else if (sortByType == "Downloaded") {
                sortByDownloaded();
                var imagetomatch="bg-single.png";
        }
        //UIALogger.logMessage("imagetomatch is "+imagetomatch);
        var window = app.mainWindow();
        var images = window.scrollViews()[libScrollViewPosition].images();
        for ( var i=0; i<images.length; i++){
                var imagename = images[i].name();
                if (imagename == imagetomatch){
                        totalCnt++;
                }
        }
        return totalCnt;
}

/*----------------------------------------
 *@tcLoopLoginLogout
 *LogsIn and LogsOut for as many times specified
 *--------------------------------------*/
function loopLoginLogout(maxNumLoops) {
    for (i=0; i< maxNumLoops; i++) {
        testLogin(username,password);
        testLogout(username);
    }
}

/*----------------------------------------
 *@tcLoopExplodedLib
 *loops from exploded-lib
 *UI: Exploded View
 *--------------------------------------*/
function loopExplodedLib(maxNumLoops) {
    for (i=0; i< maxNumLoops; i++) {
        goExplodedToLib();
        goLibToExploded();
    }
}

/*----------------------------------------
 *@tcLoopLibExploded
 *loops from lib-exploded
 *UI: Lib View
 *--------------------------------------*/
function loopLibExploded(maxNumLoops) {
    UIALogger.logMessage("in loopLibExploded");
    for (i=0; i< maxNumLoops; i++) {
        goLibToExploded();
        goExplodedToLib();
    }
}

/*
function fbShareMagazines() {
if ( app.mainWindow().scrollViews()[0].scrollViews()[0].staticTexts()["Share magazines"] != "[object UIAElementNil]" && loginWaitCount < maxCount ) {
	fbShareMagazines.tap(); 
	
*/
