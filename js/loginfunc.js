/*---------------------------------------------------*
* @ Initiates "Connect with Facebook" login process
* UI: Login Screen
*-----------------------------------------------------*/
function startLoginToFacebook(FBpassword,maxCount) {
	UIALogger.logMessage("Checking elapsed time after Connect with Facebook tap off splash");
	UIATarget.localTarget().frontMostApp().mainWindow().buttons()["  Connect with Facebook"].tap();	
	timeIt(function() {waitForfbConnectOrCreate(maxCount); });
	var fbConnectNimWindow = app.mainWindow().elements().withPredicate('name like "Connect with your Next Issue Account"');
	 var fbCreateNimWindow = app.mainWindow().elements().withPredicate('name like "Create a Next Issue Account"');
	 if (fbConnectNimWindow != "[object UIAElementNil]") { 
		var fbNimPassword = app.mainWindow().secureTextFields()[0]; 
		app.keyboard().typeString(FBpassword);
		app.mainWindow().buttons()["Sign In"].tap();
	 }

	 if (fbCreateNimWindow != "[object UIAElementNil]") { 
		var fbNimPassword = app.mainWindow().secureTextFields()[0]; 
		app.keyboard().typeString(FBpassword);
		app.mainWindow().buttons()["Checkbox Empty"].tap();
		app.mainWindow().buttons()["Sign Up"].tap();
	 }

}

/*---------------------------------------------------*
* @ Initiates "Sign in with Next Issue" login process
* UI: Login Screen
*-----------------------------------------------------*/
function startLoginToNim(username,password,app_name) {
	UIALogger.logMessage("app_name is "+app_name);
	switch (app_name) {
	case "rogers":
	  signInText="Sign in with Rogers details";
	  signInPopoverTitleText="Sign in with your Rogers account";
	  break;
	default:
	  signInText="Sign in with Next Issue details";
	  signInPopoverTitleText="Sign in with your Next Issue account";
	}
	UIATarget.localTarget().frontMostApp().mainWindow().buttons()[signInText].tap();

	//Sometimes the reader puts the text fields within a scroll view, sometimes not.
	var nimUsername = app.mainWindow().scrollViews()[0].textFields()[0];
	var nimPassword = app.mainWindow().scrollViews()[0].secureTextFields()[0];
	var signInPopoverTitle = app.mainWindow().staticTexts()[signInPopoverTitleText];

	if (nimUsername == "[object UIAElementNil]") {
		var nimUsername = app.mainWindow().textFields()[0];
	}
	if (nimPassword == "[object UIAElementNil]") {
		var nimPassword = app.mainWindow().secureTextFields()[0];
	}
	
	//Verify appropriate app_name (rogers vs NIM ) sees appropriate text on the sign in popover
	if (signInPopoverTitle == "[object UIAElementNil]") {
		UIALogger.logFail("wrong popover sign in title text seen. signInPopoverTitleText="+signInPopoverTitleText);
	}

	//Sign in using the keyboard
	nimUsername.tap();
	app.keyboard().typeString(username);
	nimPassword.tap();
	app.keyboard().typeString(password);
	target.delay(1);
	app.keyboard().typeString("\n");
	//app.mainWindow().buttons()["Sign In"].tap();
}

/*-----------------------------------------------------------*
 * @ Do we see the "first time user"  welcome banner?
 * UI: lib view
 *-----------------------------------------------------------*/
function checkForWelcomeNoEnts() {
        var welcomePopover = UIATarget.localTarget().frontMostApp().mainWindow().staticTexts()["Welcome To Next Issue"];
        if (welcomePopover != "[object UIAElementNil]") {
                UIALogger.logWarning("Got welcome *no entitlement* banner!");
                return 1;
        } else {
                //UIALogger.logMessage("no welome banner");
                return 0;
        }
}

/*-----------------------------------------------------------*
 * @ Are we in the library?
 * UI: lib view
 *-----------------------------------------------------------*/
function checkForSignedIntoLib() {
	//if (checkForWelcomeNoEnts() == 1 || app.navigationBar().buttons()["options up"] != "[object UIAElementNil]") {
	//Above not working, using below for now
	if (checkForWelcomeNoEnts() == 1 || window.navigationBar().staticTexts()["Sort By"] != "[object UIAElementNil]") {
	   //UIALogger.logMessage ("returning 1");
	   return 1;
	} else {
	   //UIALogger.logMessage ("returning 0");
	   return 0;
        }
}
/*-----------------------------------------------------------*
 * @ did we time out while signing in?
 * UI: Not applicable
 *-----------------------------------------------------------*/
function checkLoginTimeOut(loginWaitCount,maxCount) {
	if (loginWaitCount == maxCount) {
		UIALogger.logDebug("loginWaitCount is "+loginWaitCount+". maxCount is "+maxCount);
		UIALogger.logFail("event unsuccessful");
		return 0;
	} else {
		UIALogger.logPass("event successful");
		return 1;
        }
}

/*-----------------------------------------------------------*
 * @ waits until we either get the new user help overlay or library
 * UI: Sign in screen - user just tapped sign in.
 *-----------------------------------------------------------*/
function waitForLoginToFinish(maxCount) {
	var loginWaitCount=0;
        while (checkForSignedIntoLib() != 1 && loginWaitCount < maxCount ) {
		//first time user, help overlay
		if (checkForOverlay() == 1) {
			tapDoneOnOverlay();
			return 1;
		}
		UIALogger.logMessage("Not signed in yet.  Incrementing login count");
                loginWaitCount++;
        }
	checkLoginTimeOut(loginWaitCount,maxCount);
}

function waitForfbConnectOrCreate(maxCount) {
	var loginWaitCount=0;
	var fbConnectNimWindow = app.mainWindow().elements().withPredicate('name like "Connect with your Next Issue Account"');
	var fbCreateNimWindow = app.mainWindow().elements().withPredicate('name like "Create a Next Issue Account"');
        while ( fbConnectNimWindow == "[object UIAElementNil]" && fbCreateNimWindow == "[object UIAElementNil]" && loginWaitCount < maxCount ) {
		if (checkForSignedIntoLib() == 1) {
			return 1;
		}
		UIALogger.logMessage("Not signed in yet.  Incrementing login count");
                loginWaitCount++;
        }
	checkLoginTimeOut(loginWaitCount,maxCount);
}


/*-----------------------------------------------------------*
 * @ Login user user name and password
 *-----------------------------------------------------------*/
function testLogin (username,password,facebook) {
	UIALogger.logStart ("Test Login "+username);
	if (checkForSignedIntoLib() == 1) {
		UIALogger.logWarning("Already Signed in.");
		if (checkIfDesiredUserAlreadySignedIn() == 1) {
			UIALogger.logMessage("Our desired user "+username+" is already signed in.  Continuing");
			closeSettingPopup();
			return 1;
		} else {
			UIALogger.logMessage("Our desired user is not signed in.  Logging out");
			closeSettingPopup();
			testLogout(username);
		}
	}

	if (checkForEnvOverlay() ==1) {
  		 UIALogger.logMessage("We got the Env Overlay - debug build?");
       		 selectEnvOverlay(env);
		 target.delay(1);
	}

	var maxCount=30;
	if (facebook == 1) {  
		startLoginToFacebook(FBpassword,maxCount);
	} else {
		startLoginToNim(username,password,app_name);
	}
	timeIt(function() {waitForLoginToFinish(maxCount); });
	target.delay(3);
 	return 1;	 

}
	  
/*-----------------------------------------------------------*
 * @ Log out using the username 
 *-----------------------------------------------------------*/	  
function testLogout (username) {
	UIALogger.logStart ("Test Logout "+username);
	tapSetting ();
	var signout = "Sign Out: "+username;
	var signout = getLogoutIndex();
	app.mainWindow().popover().tableViews()["Empty list"].cells()[signout].tap();
	target.delay(1);
	switch (app_version) {
	    case "grenade":
		UIALogger.logDebug("Expecting sign out confirmation screen");
		app.mainWindow().popover().tableViews()["Empty list"].cells()["Yes"].tap();
		target.delay(1);
		break;
	    default:
		UIALogger.logDebug("Continuing to sign out w/out waiting for confirmation screen");
	}
}
/*-----------------------------------------------------------*
 * @ Gets the position of the sign out option.  
 * Logs a warning if the actual username signing out is different from the expected one.
 *-----------------------------------------------------------*/	  
function getLogoutIndex() {
        var cells = target.frontMostApp().mainWindow().popover().tableViews()["Empty list"].cells();
        for (var i=0; i<cells.length; i++) {
                var cellname=cells[i].name();
                var cellenabled=cells[i].isEnabled();
                if (cellname.match("Sign Out:") != null) {
                    if (cellenabled == 1) {
			if (cellname.match(username) == null) {
				UIALogger.logWarning("Expected "+username+"to be signing out, but it is someone different.");
			}
                        return i;
                    } else {
                        return -1;
                    }
                }
        }
}

/*-----------------------------------------------------------*
 * @ If the username in our js matches what is in settings, stay signed in.
 *-----------------------------------------------------------*/	  
function checkIfDesiredUserAlreadySignedIn() {
	UIALogger.logMessage("here in desired user");
        tapSetting();
        var signOutSelected=app.mainWindow().popover().tableViews()[0].elements()[getLogoutIndex()]
        var signOutString=signOutSelected.name();
        var signOutList = signOutString.split(": ");
        var current_username = signOutList[1];
        UIALogger.logMessage("current_username is "+current_username);
        if (current_username == username) {
                UIALogger.logMessage("we have a match.  stay signed in!");
                return 1;
           } else {
                UIALogger.logMessage("no match");
                return 0;
         }
}

