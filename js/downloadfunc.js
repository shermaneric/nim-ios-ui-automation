/*-----------------------------------------------------------*
 * @ selected an issue has been download yet and start
 * Progressive Download and wait till the reader comes out
 *-----------------------------------------------------------*/
function startProgressiveDownload(){
	var issues = buildTitleList();
	var openIndex = 0;

	var issueIndex = openNotDownloadedIssue (issues);
	UIALogger.logMessage ("Open issue "+issueIndex);
 

	var maxCount = 60;
	var testCount = 0;

	// Now checking if the download is completed
	if (issueIndex != -1){
		// Time how long it takes for reader to open.  Fail if it does not open.
		timeIt(function() {waitUntilReaderOpens(maxCount); });
		return 1;
	}else {
		UIALogger.logFail  ("Fail to find a new issue to open ");
		return -1;
	}	
}

/*-----------------------------------------------------------*
 * @ checks actual issue status vs. expected
 * Expected: an issue in the process of being downloaded
 * UI:  flat view.
 *-----------------------------------------------------------*/
function checkIssueStatus(selectedIn,issueStatusCheck){
	//UIALogger.logDebug("in checkIssueStatus.  I am "+issueStatusCheck);
        var issueStatusCorrect=0;

	if (app_version == "howitzer") {
                var libWindow = UIATarget.localTarget().frontMostApp().windows()[0];
                var images = libWindow.scrollViews()[libScrollViewPosition].scrollViews()[0].images();
        	var elements = libWindow.scrollViews()[libScrollViewPosition].scrollViews()[0].elements();
        } else {
                var libWindow = window;
        	var elements = libWindow.scrollViews()[libScrollViewPosition].elements();
        }
        for (  var i=0; i<elements.length; i++){
                var elementname = elements[i].name();
                var elementvalue = elements[i].value();
                var rectInfo = elements[i].rect();
                if (elementname != null) {
                    if (partOfIssue (selectedIn, rectInfo) !=0) {
		     switch (issueStatusCheck) {
			case "waiting":
	                        if (elementname == "Waiting...") {
 	                             var issueStatusCorrect=1;
				}
	                        if (elementname == "Preparing Download...") {
 	                             var issueStatusCorrect=0;
				     return issueStatusCorrect;
				}
			break;
			case "preparing":
	                        if (elementname == "Preparing Download...") {
 	                             var issueStatusCorrect=1;
				}
	                        if (elementname == "Downloading Index...") {
 	                             var issueStatusCorrect=0;
				     return issueStatusCorrect;
				}
			break;
			case "metablock":
	                        if (elementname == "Downloading Index...") {
 	                             var issueStatusCorrect=1;
				}
	                        if (elementname == "Pinning to Device..." || elementname == "Downloading Issue..."  ) {
 	                             var issueStatusCorrect=0;
				     return issueStatusCorrect;
				}
			break;
			case "archives":
	                        if (elementname == "Pinning to Device..." || elementname == "Downloading Issue...") {
 	                             var issueStatusCorrect=1;
				}
			break;
			case "opening":
	                        if (elementname == "Opening...") {
 	                             var issueStatusCorrect=1;
				}
			break;
		       }
	  	    }
	        }
	}
    return issueStatusCorrect;
}



/*-----------------------------------------------------------*
 *@ waits for the download to complete
 *UI:  flat view.
 *-----------------------------------------------------------*/
function waitDownloadCompleted(selectedIssue,maxCount) {
	//wait to see if any other issues are ahead of us.  We will wait for maxCount.
        if (waitWaitingCompleted(selectedIssue,maxCount) == 0) {
                UIALogger.logFail("Abandoning download.  We are still waiting");
        }

	//wait for Preparing download to complete, report time.
	timeIt(function() {waitIssuePartCompleted(selectedIssue,maxCount,"preparing"); });

	//wait for metablock to complete, report time.
	timeIt(function() {waitIssuePartCompleted(selectedIssue,maxCount,"metablock"); });

	//wait for archives to complete, report time.
	timeIt(function() {waitIssuePartCompleted(selectedIssue,maxCount,"archives"); });

	return 1;
}

/*-----------------------------------------------------------*
 * @ Checks to see if the download is "waiting".  Will wait until it starts downloading or times out.
 * UI:  flat view.
 *-----------------------------------------------------------*/
function waitWaitingCompleted(selectedIssue,maxCount) {
        UIALogger.logStart("in waitWaitingCompleted");
        var testCount = 0;
	var waitingInProg=0;

        //Stay in while loop until the "Waiting" messaging goes away
        var waitingInProg = checkIssueStatus(selectedIssue,"waiting");
        if (waitingInProg == 1) {
                while (waitingInProg == 1 && testCount < maxCount) {
                        target.delay(1);
                        waitingInProg = checkIssueStatus(selectedIssue,"waiting");
                        testCount++;
                }
                if (testCount == maxCount) {
                        UIALogger.logMessage("we have waited too long.");
                        return 0;
                } else {
                        return 1;
                }
        } else {
                UIALogger.logMessage("We didn't have any Waiting treatment on the thumbnail.");
                return 1;
        }
      UIALogger.logMessage("leaving waitWaitingCompleted");
}



/*-----------------------------------------------------------*
 * @ Checks to see if the metablock or archives are completed
 * UI:  flat view.
 *-----------------------------------------------------------*/
function waitIssuePartCompleted(selectedIssue,maxCount,issuePart) {
        var testCount = 0;
        var curr_num_waits=0;
        var max_num_waits=10;

        //Stay in while loop until thumbnail text changes to what we are looking for

        var myIssuePartInProg = checkIssueStatus(selectedIssue,issuePart);
        while (myIssuePartInProg == 0 && testCount<maxCount && curr_num_waits < max_num_waits ) {
                target.delay(1);
                var myIssuePartInProg = checkIssueStatus(selectedIssue,issuePart);
                testCount++;
		curr_num_waits++;
        }

        if (testCount==maxCount || curr_num_waits==max_num_waits) {
                UIALogger.logMessage ("we never saw the "+issuePart+" start downloading. Pinned an already d/lded issue?");
		return 0;
        } else {
                UIALogger.logStart("The "+issuePart+" now in progress");
        }

        testCount=0;
        var myIssuePartInProg = checkIssueStatus(selectedIssue,issuePart);
        while (myIssuePartInProg == 1 && testCount<maxCount ) {
                target.delay(1);
                var myIssuePartInProg = checkIssueStatus(selectedIssue,issuePart);
                testCount++;
        }
        if (testCount==maxCount ) {
                UIALogger.logMessage ("we never saw the "+issuePart+" finish after "+maxCount+" seconds.  Quick issue?");
        } else {
                UIALogger.logMessage("The "+issuePart+ " finished.");
        }
}
