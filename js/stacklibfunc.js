/*-----------------------------------------------------------*
 * @ From the stack lib view, build a list of titles.
 * If the screen gor refreshed, we need to rebuild it, because the
 * array is a list of thumbnail, no title name associated with it.
 * UI: Flat Lib View 
 * Return: array of issueObject
 *-----------------------------------------------------------*/
function builStackTitleList(){
	var titles = new Array();
	//window.logElementTree();
	var images = window.scrollViews()[scrollViewPosition].images();
	var totalCnt = 0;
	for (var  i=0; i<images.length; i++){
		var imagename = images[i].name();
		if (imagename == "bg-stack.png"){
			var newIssue = new issueObject (images[i].rect());
			titles[totalCnt]=newIssue;
			totalCnt++;
		}
	}
	UIALogger.logMessage ("Total Title Created "+totalCnt);
	return titles;
}
/*-----------------------------------------------------------*
 * @ From the stack lib view, go through each stack to find the pinned
 * and download issue number.
 * UI: Lib View (Flat or Stack)
 * Return: array of totalDownloaded, totalunpinned, totalpinned
 *-----------------------------------------------------------*/
function getUsageCountFromLib(){
	sortByTitle();
	var totalTitles = builStackTitleList();
	var titleLen = totalTitles.length;
	var totalPinned = 0;
	var totalUnpinned = 0;
	var totalDownloaded = 0;
	for ( var i=0;i<titleLen; i++){
		var title = totalTitles[i];
		// Tap on each title
		var usageInExpldedView = GetUsageCountFromExploded (title);
		UIALogger.logMessage ("Title "+i+" usage "+ usageInExpldedView);
		totalDownloaded += usageInExpldedView[0];
		totalUnpinned += usageInExpldedView[1];
		totalPinned += usageInExpldedView[2];
	}
	return [totalDownloaded, totalUnpinned, totalPinned];
}
/*-----------------------------------------------------------*
 * @ Go through exploded view and find the usage for each title
 * selectedTitle : The title object 
 * UI: Stack Lib View
 * Return: array of totalDownloaded, totalunpinned, totalpinned
 *-----------------------------------------------------------*/


function GetUsageCountFromExploded (selectedTitle){
	UIATarget.localTarget().tap({x: selectedTitle.x1+10, y: selectedTitle.y1+10});
	target.delay(3);
	
	// Now build the issue list
	var usageCount = getUsageFromIssueList();
	// Tab back to Lib
	app.navigationBar().buttons()["All Titles"].tap();	
	target.delay(3);
	
	return usageCount;

			
}
