/*-----------------------------------------------------------*
 * @ Tap the Done button in Picker View 
 * UI: Picker View-> Lib View
 * ESS 8/30.  Created "doneButton".
 *-----------------------------------------------------------*/
function tapDoneInPickerview (){
	var doneButton = UIATarget.localTarget().frontMostApp().navigationBar().buttons()["Done"];
	if (doneButton.isVisible()) {
		UIALogger.logMessage("it is visible")
		doneButton.tap();
		target.delay(2);
	} else {
		UIALogger.logMessage("it is not visible. wait. ")
		doneButton.tap();
		target.delay(2);
	}
	target.delay(3);
}


/*-----------------------------------------------------------*
 * @ Finds the specified title in picker view
 * selectTitleName, string of the title
 * UI: Picker View, the selected title is looked for
 *
 *-----------------------------------------------------------*/
function findTitleInPickerview (selectTitleName){
	target.delay(2);
        var window = app.mainWindow();
        var currentLastTitle = getLastTitleInView ();
	UIALogger.logMessage("a: "+selectTitleName+" b: "+currentLastTitle);
        var isInView = checkIfTitleShown (currentLastTitle, selectTitleName);
        while (isInView == 0){
                UIALogger.logMessage("isInView is 0");
                window.dragInsideWithOptions({startOffset:{x:0.50, y:0.8}, endOffset:{x:0.50, y:0.6}, duration:0.2});
                target.delay(3);
                var previousLastTitle = currentLastTitle;
                currentLastTitle = getLastTitleInView ();
                if (previousLastTitle == currentLastTitle){
                        UIALogger.logFail ("No more titles");
                        break;
                }
		UIALogger.logMessage("a: "+selectTitleName+" b: "+currentLastTitle);
                isInView = checkIfTitleShown (currentLastTitle, selectTitleName);
        }

        if (isInView == 1){
                UIALogger.logMessage("Title "+selectTitleName+" is in picker view");
		//Temporary.  makes sure the title can be selected. 2013-07-22
		//Take out when we can
                //window.dragInsideWithOptions({startOffset:{x:0.50, y:0.8}, endOffset:{x:0.50, y:0.75}, duration:0.2});
		return 1;
        }
        if (isInView == 0){
                // Can't find it, fail
                UIALogger.logFail ("Fail to Find the title in picker view");
                return 0;
        }
}

/*-----------------------------------------------------------*
 * @ Tap the specified title in Picker View 
 * selectTitleName, string of the title
 * UI: Picker View, the selected title is tap  (from unselected to select or Vice versa) 
 *-----------------------------------------------------------*/
function tapTitleInPickerview (selectTitleName,bTapTitle){
	findTitleInPickerview (selectTitleName);
	// Now it is in sight, go get it!!
	var matchName = "name like '"+selectTitleName+"'";

	//get the rect info and use target
	var titleStaticText = getTitleShownInPickerView (matchName);
	if ( titleStaticText.toString() != "[object UIAElementNil]" ) {
		UIALogger.logMessage ("found the title");
		
		if (titleStaticText.isVisible() == 0) {
			UIALogger.logMessage ("bring to visible");
			target.delay(2);
			titleStaticText = getTitleShownInPickerView(matchName);
		}
		titleStaticText.logElement();
		if (bTapTitle == 1) {
		UIATarget.localTarget().touchAndHold(titleStaticText.rect(),1);
		}
		return 1;
	} else {
		UIALogger.logWarning("We did not get our Title!");
		return 0;
	}
}
/*-----------------------------------------------------------*
 * @ Tap the specified title in Picker View 
 * selectTitleName, string of the title
 * UI: Picker View, the selected title is tap  (from unselected to select or Vice versa) 
 *-----------------------------------------------------------*/
function getTitleShownInPickerView (matchName){
	var window = app.mainWindow();
	var elementcount = window.scrollViews()[0].elements().length;
	UIALogger.logMessage ("element count="+elementcount);

	var titleStaticText = window.scrollViews()[0].elements().firstWithPredicate(matchName);
	var myTitleString = titleStaticText.toString();
	UIALogger.logMessage("myTitleString is "+myTitleString);
	return titleStaticText;
}

function checkIfTitleShown (currentLastTitle, selectedName){
	UIALogger.logMessage ("currentLastTitle="+currentLastTitle+", selectedName="+selectedName);
	if (selectedName > currentLastTitle){
		UIALogger.logMessage ("Not in the range");
		return 0;
	}
	UIALogger.logMessage ("in the range");
	return 1;
}

function getLastTitleInView(){
	var titleStaticTexts = window.scrollViews()[libScrollViewPosition].staticTexts();
	UIALogger.logMessage ("total element="+ titleStaticTexts.length);
	for ( var i=titleStaticTexts.length; i--;){
		UIALogger.logMessage (titleStaticTexts[i].name());
		var str = titleStaticTexts[i].name();
		if (str == null){
			UIALogger.logMessage (" No name "+titleStaticTexts[i]);
			continue;
		}
		var n = str.charAt(0);
		if (n!='<'){
			UIALogger.logMessage (" Las Title is... "+str);
			return str;
		}	
	}
}

