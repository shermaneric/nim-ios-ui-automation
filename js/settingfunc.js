


/*-----------------------------------------------------------*
 * @ Sing out with user username 
 * UI: Lib View, or Content View -> User Logout, Sign In Page
 *-----------------------------------------------------------*/
function logout (username) {
	var buttons = app.navigationBar().buttons();
	var len = buttons.length;
	for (var i=0; i<len; i++)
		{
			buttons[i].logElement();
		}
	app.navigationBar().buttons()["options up"].tap();
	target.delay(2);
	var signout = "Sign Out: "+username;
	app.mainWindow().popover().tableViews()["Empty list"].cells()[signout].tap();
}
/*-----------------------------------------------------------*
 * @ Bring up the Setting Popup
 * UI: Lib View, or Content View -> Setting Popup
*-----------------------------------------------------------*/
function tapSetting (){

	// Made a catch to use "options up" if for some reason (like in the reader) buttons()[2] is not visible
	if (NIMiPad == 1 && app.navigationBar().buttons()[2].isVisible() != 0) {
		app.navigationBar().buttons()[2].tap();
	} else{
		app.navigationBar().buttons()["options up"].tap();
	}

	target.delay(2);
}
/*-----------------------------------------------------------*
 * Verifies setting popup there, return 1 (yes); 0 (no)
 * UI: Lib or reader.
*-----------------------------------------------------------*/
function checkIfSettingsButtonExists (){
	// On Simulator, we can user "options up", but it does not work on device
	// Made a catch to use "options up" if for some reason (like in the reader) buttons()[2] is not visible
	if (NIMiPad == 1 && app.navigationBar().buttons()[2].isVisible() != 0) {
		return 1;
	} else  if (app.navigationBar().buttons()["options up"].isVisible() != 0) {
		return 1;
	} else {
		return 0;
	}
}

/*-----------------------------------------------------------*
 * Verifies setting popup there, return 1 (yes); 0 (no)
 * UI: Lib or reader.
*-----------------------------------------------------------*/
function checkIfSettingsPopoverExists () {
	if (app.mainWindow().popover().navigationBar()["Settings"] != "[object UIAElementNil]") {
		return 1;
	} else {
		return 0;
	}
}

/*-----------------------------------------------------------*
 * @ Bring up the Usage Popup
 * UI: Setting Popup -> Usage Popup
*-----------------------------------------------------------*/
function tapUsage (){
	app.mainWindow().popover().tableViews()["Empty list"].cells()["Usage"].tap();
	target.delay(2);
}

/*-----------------------------------------------------------*
 * @ Bring up the Usage Popup
 * UI: Setting Popup -> Usage Popup
*-----------------------------------------------------------*/
function tapDebug (){
	app.mainWindow().popover().tableViews()["Empty list"].cells()["Debug"].tap();
	target.delay(2);
}

function getMetrics() {
	tapSetting();
	app.mainWindow().popover().tableViews()["Empty list"].cells()["Debug"].tap();
	target.delay(1);
	app.mainWindow().popover().tableViews()["Empty list"].cells()["E-Mail Metrics"].buttons()[0].tap();
	metricsElements=app.mainWindow().scrollViews()[1].elements();

	app.mainWindow().scrollViews()[1].textFields()[0].tap();
	app.keyboard().typeString("shermaneric@gmail.com\n")
	target.delay(3);
	app.mainWindow().navigationBar().buttons()["Send"].tap();
	
	
	metricsText=app.mainWindow().scrollViews()[1].elements();
	var textLen = metricsText.length;
	UIALogger.logMessage("length is "+textLen);

        for (var i=0; i<textLen; i++) {
                var eachText = metricsText()[i].name();
		UIALogger.logMessage("-"+eachElem);
        }

}

/*-----------------------------------------------------------*
 * @ Toggle the Email Notification Setting 
 * UI: Setting popup->Notificcation Popup->Setting popup
 * Return the previous setting of notification 
 *-----------------------------------------------------------*/
function switchEmailEnable (){
	app.mainWindow().popover().tableViews()["Empty list"].cells()["Notifications"].tap();
	var receiveemail =	app.mainWindow().popover().tableViews()["Empty list"].cells()["Email"].switches()["Email"].value();
	var newFlag = 0;
	if  (receiveemail==0 ){
		// Switch it to off
		UIALogger.logMessage ("receiveemail is int 0, change to 1" );
		newFlag = 1;
	}else if ( receiveemail==1 ){	
		// Switch it to off
		UIALogger.logMessage ("receiveemail is int 1, change to 0" );
		newFlag = 0;
		}
	app.mainWindow().popover().tableViews()["Empty list"].cells()["Email"].switches()["Email"].setValue(newFlag);
	
	app.mainWindow().popover().navigationBar().buttons()["Settings"].tap();
	return 	receiveemail;
}
/*-----------------------------------------------------------*
 * @ Hard code for Usage Max Issue cell index, 
 * Can't find the element useing Predicate..., so hard code it for now...
 * var maxissue = app.mainWindow().popover().tableViews()[0].elements().withPredicate('name like "Maximum"');
 * Return the hard code index
 *-----------------------------------------------------------*/
function usageMaxIssueIndex (){ 
	return 2;
}
/*-----------------------------------------------------------*
 * @ Get the Max Issue setting in Usage Popup 
 * UI: Usage Popup 
 * Return the The String of the current Max Issue Setting (16, 24, ..No Maximum)
 *-----------------------------------------------------------*/
function getUsageMaxIssueNum (){
	var maxIssue = app.mainWindow().popover().tableViews()[0].elements()[usageMaxIssueIndex ()];
	var nameString = maxIssue.name();

	var maxIssueSize = nameString.split(", ");
	var size = maxIssueSize[1];
	UIALogger.logMessage ("max size = "+ size);
	return size;

}
/*-----------------------------------------------------------*
 * @ Tap on the Usage Max Issue cell to enter the "Max Issue" popup 
 * UI: Usage Popup -> Maximum Issues popup
 *-----------------------------------------------------------*/
function tapMaxIssues (){
	app.mainWindow().popover().tableViews()[0].elements()[usageMaxIssueIndex()].tap();
	target.delay(2);
}
/*-----------------------------------------------------------*
 * @ retrieve the max issue options in Max Issue Popup
 * UI: Maximum Issues popup
 * Return: Array List of the Max Issue choices [16, 24, ..No Maximum]
 *-----------------------------------------------------------*/
function getMaxIssueOptions (){
	var maxSettingElements = app.mainWindow().popover().tableViews()[0].elements();
	var eleLen = maxSettingElements.length;
	var totalSetting = new Array();
	for (var i=0; i<eleLen; i++) {
		var eachNum = app.mainWindow().popover().tableViews()[0].elements()[i];
		totalSetting[i] = eachNum.name();
	}
	return totalSetting;
}
/*-----------------------------------------------------------*
 * @ retrieve current setting in index
 * UI: Maximum Issues popup
 * Return: urrent setting index in integer 
 *-----------------------------------------------------------*/
function getCurrentMaxIssueIndex (){
	var maxSettingElements = app.mainWindow().popover().tableViews()[0].elements();
	var eleLen = maxSettingElements.length;
	var currentSetting = -1;
	for (var i=0; i<eleLen; i++) {
		//eachNum.logElement();
		var eachNum = app.mainWindow().popover().tableViews()[0].elements()[i];
		var selected = eachNum.value();
		if (selected == "1"){
			currentSetting = i;
		}
	}
	return currentSetting;
}
/*-----------------------------------------------------------*
 * @ test the Max Issue Number values in Max Issue Popup
 * size: the string used to check with current selection in Max Issue Popup
 * UI: Maximum Issues popup
 *-----------------------------------------------------------*/
function testIssueNumber ( size){
	
	var totalSetting = getMaxIssueOptions ();
	var currentSetting = getCurrentMaxIssueIndex ();
	
	// Check if current selection match
	if (currentSetting == -1){
		UIALogger.logFail ("Missing current selection ");
		return;
	}else {
		UIALogger.logMessage ("Pass: has current setting ");
	}
	
	if (totalSetting.length!=5){
		UIALogger.logFail ("Usage Setting should have 5 rows ");
		return;
	}else {
		UIALogger.logMessage ("Pass: Usage contains 5 rows ");
	}

	// Check if contains the no macmium 
	if (totalSetting[4] != "No Maximum"){
		UIALogger.logFail ("Usage should have 'No Maxmium' as last choice ");
		return;
	}else {
		UIALogger.logMessage ("Pass:  Usage contains Maxmium ");
	}

	if (size!= totalSetting[currentSetting]){
		UIALogger.logFail ("Current Setting should be "+ size + ", not "+ totalSetting[currentSetting]);
		return;
	}else {
		UIALogger.logPass ("current setting is correct ");
	}

	// Check if the value is all current
	//12 / 24 / 48 (DEFAULT) / 72 / No Maximum
	//24 /48 / 96 (DEFAULT) / 144 / No Maximum
	//48 / 96 / 192 (DEFAULT) / 288 / No Maximum
	var totalSettingNum = new Array();
	for (var i=0; i<4 ; i++) {
		totalSettingNum[i] = parseInt(totalSetting[i]);	
	}
	var expectedNum = new Array();
	if (totalSettingNum[0] == 12){
		expectedNum = [24, 48, 72];
	}else if (totalSettingNum[0] == 24){
		expectedNum = [48, 96, 144];
	}else if (totalSettingNum[0] == 48){
		expectedNum = [96, 192, 288];
	}	else{
			UIALogger.logFail("The first number is incorrect "+totalSettingNum[0]);
			return;
	}	

	var errorItem = -1;
	for (var i=1; i<4 && errorItem == -1; i++) {
		UIALogger.logMessage ("check if "+ expectedNum[i-1] +" match "+ totalSettingNum[i]);
		if (expectedNum[i-1] != totalSettingNum[i]){
			errorItem = i;
			//throw "stop execution";
		}
	}
	if (errorItem !=-1){
		UIALogger.logFail ("The "+ errorItem + " item should be "+expectedNum[errorItem-1]+", not "+ totalSetting[errorItem]);
		// Don't call the stop exection here, or the error will not be logged
		//throw "stop execution";
	
	}else {
		UIALogger.logMessage ("Pass:  The issue number are correct ");
	}
}
/*-----------------------------------------------------------*
 * @ test Chagne the Max Issue 
 * UI: Usage popup->Max Issues popup->Usage popup
 *-----------------------------------------------------------*/
function testChangeMaxIssueNumber (){
	tapMaxIssues ();
	var totalSetting = getMaxIssueOptions ();
	var currentSettingIndex = getCurrentMaxIssueIndex() ;
	UIALogger.logMessage ("current setting is "+ totalSetting[currentSettingIndex]);
	var newSettingIndex = (currentSettingIndex + 1)%5;
	UIALogger.logMessage ("want to change to  "+ totalSetting[newSettingIndex]);

	app.mainWindow().popover().tableViews()[0].elements()[newSettingIndex].tap();
	target.delay(2);
	// After tapping, the view will go back to the Usage View 
	var afterTapSetting = getUsageMaxIssueNum ();
	if (afterTapSetting != totalSetting[newSettingIndex]){
		UIALogger.logMessage ("after Tapping "+ afterTapSetting);
		UIALogger.logMessage ("plan to change to "+ totalSetting[newSettingIndex]);
		UIALogger.logFail ("Did not change to new "+ totalSetting[newSettingIndex]);
	}else {
		UIALogger.logMessage ("Pass: Change new setting to"+ totalSetting[newSettingIndex]);
	}	
}
function closeSettingPopup(){
	var settingX = 377;
	var settingY = 45;
	//var settingX = 37;
	//var settingY = 985;
	//target.tap({x: settingX, y: settingY});
	//UIALogger.logDebug("waiting a few extra seconds in case timing problems");
	//target.delay(1);

	//If settings button still exists, tap it again
	//if (checkIfSettingsButtonExists() == 1) {
	if (checkIfSettingsPopoverExists() == 1) {
		target.tap({x: settingX, y: settingY});
		target.delay(1);
	}
}
function getUsageIssueNumbers (){
	tapSetting();
	tapUsage();
	NIMUsageDownloaded = getDownloadedIssueNumber();
	NIMUsageUnpinned = getUnpinnedIssueNumber ();
	NIMUsagePinned = getPinnedIssueNumber ();

	
	closeSettingPopup();
}
function getPinnedIssueNumber (){
	return getNumberFromTableElement (usagePinnedIssueIndex());
}
function getUnpinnedIssueNumber (){
	return getNumberFromTableElement (usageUnpinnedIssueIndex());
}
function getDownloadedIssueNumber (){
	return getNumberFromTableElement (usageDownloadedIssueIndex());
}
function usageDownloadedIssueIndex (){ 
	return 1;
}
function usageUnpinnedIssueIndex (){ 
	return 4;
}
function usagePinnedIssueIndex (){ 
	return 5;
}
function getNumberFromTableElement (tableIndex){
	var valueString = getNumberStringFromTableElement(tableIndex);
	
	// Now convert the valueString to int 
	return parseInt(valueString);
	
}
function getNumberStringFromTableElement (tableIndex){
	//var elementSelected = app.mainWindow().logElementTree();
	var elementSelected = app.mainWindow().popover().tableViews()[0].elements()[tableIndex];
	var nameString = elementSelected.name();

	var tmpArray = nameString.split(", ");
	var numberValue = tmpArray[1];
	UIALogger.logMessage ("number value = "+ numberValue);
	
	return numberValue;

}

/*-----------------------------------------------------------*
 * @ Bring up the Help Overlay
 * UI: Setting Popup
*-----------------------------------------------------------*/
function tapHelp (){
        app.mainWindow().popover().tableViews()["Empty list"].cells()["Help"].tap();
        target.delay(2);
}


/*-----------------------------------------------------------*
 * Checks to see if an overlay with the "Done" button exists
 * UI: Setting Popup -> Help
*-----------------------------------------------------------*/
function checkForOverlay () {
	UIALogger.logMessage("here in checkForOverlay");
        if (app.navigationBar().buttons()["Done"].isVisible()) {
                UIALogger.logWarning ("Overlay button is visible! First time installing app?");
		return 1;
        } else {
		UIALogger.logWarning ("no overlay.  not first time user?");
		return 0
	}
}

/*-----------------------------------------------------------*
 * Checks to see if the Environment overlay at initial app launch exists.
 * Should be on Debug builds only.
*-----------------------------------------------------------*/
function checkForEnvOverlay () {
   //var first_env=window.tableViews()[0].cells()[0].name();
   var first_env=window.tableViews()[0].cells()[1].name();
   if (first_env != null ) {
	   if (first_env.match("https") != null) {
		UIALogger.logMessage("env overlay found");
		return 1;
    	} else {
		UIALogger.logMessage("env overlay NOT found");
		return 0;
    	}	
   }
}

/*-----------------------------------------------------------*
 * gets the Index position from the Environment login overlay based on the env variable
*-----------------------------------------------------------*/
function getOverlayIndex() {
        var cells = target.frontMostApp().mainWindow().tableViews()["Empty list"].cells();
        for (var i=0; i<cells.length; i++) {
                var cellname=cells[i].name();
                var cellenabled=cells[i].isEnabled();
                if (cellname.match(env) != null) {
                    if (cellenabled == 1) {
                        return i;
                    } else {
                        return -1;
                    }
                }
        }
}
/*-----------------------------------------------------------*
 * Selects desired Env Overlay and taps Done
*-----------------------------------------------------------*/
function selectEnvOverlay (env) {
	UIALogger.logMessage("in selectEnvOverlay.  env is "+env);
	window.tableViews()["Empty list"].cells()[getOverlayIndex(env)].tap();
        app.navigationBar().buttons()["Done"].tap();

}

/*-----------------------------------------------------------*
 * @ Taps "Done" on Generic Overlay
 * UI: Any overlay with a "Done" button.
*-----------------------------------------------------------*/
function tapDoneOnOverlay () {
        if (app.navigationBar().buttons()["Done"].isVisible()) {
       	    app.navigationBar().buttons()["Done"].tap();
	} else if (window.buttons()["Done"].isVisible())  {
		window.buttons()["Done"].tap();
	} else {
	    //UIALogger.logWarning("No overlay to tap?");
	}
}
	

/*-----------------------------------------------------------*
 *Obtains the current settings options displayed on the screen
 * UI: Setting Popup
*-----------------------------------------------------------*/
function getSettingsOptions () {
        var settingElements = app.mainWindow().popover().tableViews()[0].elements();
        var eleLen = settingElements.length;
        var settingList = new Array();
        for (var i=0; i<eleLen; i++) {
                var eachNum = app.mainWindow().popover().tableViews()[0].elements()[i];
                settingList[i] = eachNum.name();
                //UIALogger.logMessage("item is "+settingList[i]);
        }
        return settingList;
}



/*-----------------------------------------------------------*
 *Verify Expected vs. Current Settings Options
 * UI: Setting Popup
*-----------------------------------------------------------*/
function verifySettingsOptions() {
        var signoutString="Sign Out: "+username;
	switch (app_version) {
	case "grenade":
		if (ipa_version=="debug" && app_name != "rogers") {
        		var expectedSettingsOptions = new Array("Cellular Settings","Notifications","Usage","Help","Legal Notices","Debug","Connect Facebook",signoutString);
		} else {
        		var expectedSettingsOptions = new Array("Cellular Settings","Notifications","Usage","Help","Legal Notices","Connect Facebook",signoutString);
		}
		break;
	case "cannonball":
        		var expectedSettingsOptions = new Array("Notifications","Usage","Help","Legal Notices",signoutString);
		break;
	default:
		if (ipa_version=="debug") {
        		var expectedSettingsOptions = new Array("Notifications","Usage","Help","Legal Notices","Debug","Connect Facebook",signoutString);
		} else {
        		var expectedSettingsOptions = new Array("Notifications","Usage","Help","Legal Notices","Connect Facebook",signoutString);
		}
	}

        //tapSetting();
        var currentSettingsOptions = getSettingsOptions();
        for (var i=0; i<currentSettingsOptions.length; i++) {
                 //UIALogger.logMessage("Current Settings option "+i+" is "+currentSettingsOptions[i]);
                 //UIALogger.logMessage("Expected Settings option "+i+" is "+expectedSettingsOptions[i]);
                 if (currentSettingsOptions[i] != expectedSettingsOptions[i]) {
                        UIALogger.logFail ("Settings Options do not match!");
                 	UIALogger.logFail ("Current Settings option "+i+" is "+currentSettingsOptions[i]);
	                UIALogger.logFail ("Expected Settings option "+i+" is "+expectedSettingsOptions[i]);
                } else { 
                        UIALogger.logPass ("Settings Options Match! "+currentSettingsOptions[i]);
		}
        }
}

/*-----------------------------------------------------------*
 * Get the "All Unpinned position off the Usage Screen.
 * Returns -1 if disabled (All Pinned = 0)
 * UI:  Usage Popup
*-----------------------------------------------------------*/
function getPinnedIndex() {
        var cells = target.frontMostApp().mainWindow().popover().tableViews()["Empty list"].cells();
        for (var i=0; i<cells.length; i++) {
                var cellname=cells[i].name();
                var cellenabled=cells[i].isEnabled();
                if (cellname.match("All Pinned") != null) {
                    if (cellenabled == 1) {
                        return i;
                    } else {
                        return -1;
                    }
                }
        }
}

/*-----------------------------------------------------------*
 * Get the "All Unpinned position off the Usage Screen.
 * Returns -1 if disabled (All Unpinned = 0)
 * UI:  Usage Popup
*-----------------------------------------------------------*/
function getUnpinnedIndex() {
        var cells = target.frontMostApp().mainWindow().popover().tableViews()["Empty list"].cells();
        for (var i=0; i<cells.length; i++) {
                var cellname=cells[i].name();
                var cellenabled=cells[i].isEnabled();
		//UIALogger.logDebug("cellname is "+cellname);
		//UIALogger.logDebug("cellenabled is "+cellenabled);
                if (cellname.match("All Unpinned") != null) {
                    if (cellenabled == 1) {
                        return i;
                    } else {
                        return -1;
                    }
                }
        }
}

/*-----------------------------------------------------------*
 @ deletes Unpinned issues if confirm = yes
 * goes to the delete window and presses no if confirm = no
 * UI:  Usage Popup
*-----------------------------------------------------------*/
function deleteUnpinnedIssues(confirm) {
	tapSetting();
        tapUsage();
        target.delay(1);
        var unpinnedIndex=getUnpinnedIndex();
        if (confirm != "No" && confirm != "Yes") {
                UIALogger.logFail ("change your script to confirm=No or Yes");
        }
        if (unpinnedIndex != -1) {
                var allUnpinnedCell= target.frontMostApp().mainWindow().popover().tableViews()["Empty list"].cells()[unpinnedIndex];
                allUnpinnedCell.tap();
                target.delay(1);

                target.frontMostApp().mainWindow().popover().buttons()[confirm].tap();
                if (confirm == "Yes") {
			waitForSettingsDeleteToFinish();
                } else {
                        target.delay(1);
                }
        } else {
                UIALogger.logMessage ("No unpinned issues to delete?");
        }
	closeSettingPopup();
}


/*-----------------------------------------------------------*
 @ deletes Pinned issues if confirm = yes
 * goes to the delete window and presses no if confirm = no
 * UI:  Usage Popup
*-----------------------------------------------------------*/
function deletePinnedIssues(confirm) {
	tapSetting();
        tapUsage();
        target.delay(1);
        var pinnedIndex=getPinnedIndex();
        if (confirm != "No" && confirm != "Yes") {
                UIALogger.logFail ("change your script to confirm=No or Yes");
        }
        if (pinnedIndex != -1) {
                var allPinnedCell= target.frontMostApp().mainWindow().popover().tableViews()["Empty list"].cells()[pinnedIndex];
                allPinnedCell.tap();
                target.delay(1);
                target.frontMostApp().mainWindow().popover().buttons()[confirm].tap();
                if (confirm == "Yes") {
			waitForSettingsDeleteToFinish();
			UIALogger.logDebug("waiting for a while due to long nature of delete");
			target.delay(15);
                } else {
                        target.delay(1);
                }
        } else {
                UIALogger.logMessage ("No unpinned issues to delete?");
        }
	closeSettingPopup();
}

function waitForSettingsDeleteToFinish() {
	var maxWaitForDelete=60;
	var deleteCount=0;
	while (target.frontMostApp().mainWindow().popover().buttons()["Yes"].isVisible() && deleteCount <maxWaitForDelete ) {
		target.delay(1);
	}
	if (deleteCount == maxWaitForDelete) {
		UIALogger.logFail("Issues took more than"+maxWaitForDelete+" seconds to delete!");
	}
}


/*-----------------------------------------------------------*
 * @ In app disconnect to facebook
 * UI: FB signed in through iOS 6.
 *-----------------------------------------------------------*/
function disconnectFacebook() {
	var maxCount=30;
	tapSetting();
        app.mainWindow().popover().tableViews()["Empty list"].cells()["Disconnect Facebook"].tap();
	switch (app_version) {
            case "grenade":
                UIALogger.logDebug("Expecting sign out confirmation screen");
                app.mainWindow().popover().tableViews()["Empty list"].cells()["Yes"].tap();
                target.delay(1);
                break;
            default:
                UIALogger.logDebug("Continuing to sign out w/out waiting for confirmation screen");
        }

	timeIt(function() {waitForDisconnectToFinish(maxCount); });
	return waitForDisconnectToFinish(maxCount); 
}

/*-----------------------------------------------------------*
 * @ In app disconnect to facebook
 * UI: called by disconnectFacebook();
 *-----------------------------------------------------------*/
function waitForDisconnectToFinish(maxCount) {
        var loginWaitCount=0;
        while (app.mainWindow().scrollViews()[libScrollViewPosition].scrollViews()[0].staticTexts()["Selection of free magazines"] != "[object UIAElementNil]" && loginWaitCount < maxCount ) {
                loginWaitCount++;
        }
        return checkLoginTimeOut(loginWaitCount,maxCount);
}

/*-----------------------------------------------------------*
 * @ In app connect to facebook
 * UI: FB signed in through iOS 6.
 *-----------------------------------------------------------*/
function connectFacebookInApp() {
	var maxCount=30;
	tapSetting();
        app.mainWindow().popover().tableViews()["Empty list"].cells()["Connect Facebook"].tap();
	timeIt(function() {waitForInAppConnectToFinish(maxCount); });
	return waitForInAppConnectToFinish(maxCount);
}

/*-----------------------------------------------------------*
 * @ In app connect to facebook
 * UI: called by connectFacebook();
 *-----------------------------------------------------------*/
function waitForInAppConnectToFinish(maxCount) {
        var loginWaitCount=0;
        while (app.mainWindow().scrollViews()[libScrollViewPosition].scrollViews()[0].staticTexts()["Selection of free magazines"] == "[object UIAElementNil]" && loginWaitCount < maxCount ) {
                loginWaitCount++;
		target.delay(1);
        }
        return checkLoginTimeOut(loginWaitCount,maxCount);
}
