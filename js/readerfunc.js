
/*-----------------------------------------------------------*
 * @ Check if current UI is reader view 
 *-----------------------------------------------------------*/
function isInReader(){
        //Check for Help Overlay
        if (app.navigationBar().buttons()["Done"].isVisible()) {
                UIALogger.logWarning ("Help Overlay button is visible!  First time installing app?");
                 tapDoneOnOverlay();
        }

	var libtext = app.navigationBar().staticTexts()["Library"];
	if (libtext.toString() != "[object UIAElementNil]") {
		UIALogger.logMessage ("In Reader");
		return 1;
	}else {
		UIALogger.logMessage ("Not In Reader");
		return 0;
	}
}	

function waitUntilReaderOpens(maxCount) {
	var readerOpened=0;
	var testCount=0;
        while (readerOpened ==0 && testCount<maxCount) {
                        target.delay(1);
                        readerOpened = isInReader();
                        testCount++;
                }

         if (readerOpened == 1){
              UIALogger.logPass  ("Enter Reader.  Waiting extra time to dismiss auto dl alert");
              target.delay(3);
              return 1;
         }else {
              UIALogger.logFail  ("Fail to enter reader after "+maxCount+ "seconds.");
              return 0;
        }
}

/*-----------------------------------------------------------*
 * @ show Chrome
 * UI : in reader view 
 *-----------------------------------------------------------*/
function showChrome(autoSwipeForward) {
       //we used autoSwipeForward to try and swipe forward if stuck.  now no longer necessary
       //therefore, showChrome and showChromeNoSwipe are the same
       UIALogger.logMessage("in showChrome");
       var chromeExists=0;
       var indexOverlayButton = target.frontMostApp().navigationBar().buttons()["index up"];
       var chromeCheckCount = 1
       var chromeMaxCount = 8;
        while (indexOverlayButton.isVisible() == 0 && chromeCheckCount < chromeMaxCount) {
                var indexOverlayButton = target.frontMostApp().navigationBar().buttons()["index up"];
                if (indexOverlayButton.isVisible() == 1 ) {
                        UIALogger.logMessage("Chrome now seen!");
                        return 1;
                   } else {
                        UIALogger.logMessage("No Chrome yet.");
			//tapDoneOnOverlay();
                        //window.scrollViews()[0].scrollViews()[0].tapWithOptions({tapOffset:{x:offsetForChromeX, y:offsetForChromeY}});
			target.tap({x:348.00, y:342.00});
			target.delay(1);
                        chromeCheckCount++;
                  }
        }
        if (chromeCheckCount == chromeMaxCount) {
                UIALogger.logFail("Chrome Never Seen!");
		return 0;
        }
	UIALogger.logMessage("Chrome Now seen!");
        return 1;
}



function dismissChrome() {
       UIALogger.logMessage("in dismissChrome");
       var indexOverlayButton = target.frontMostApp().navigationBar().buttons()["index up"];
        if (indexOverlayButton.isVisible() == 1) {
			UIALogger.logMessage("dismissing chrome");
			window.scrollViews()[0].scrollViews()[0].tapWithOptions({tapOffset:{x:offsetForChromeX, y:offsetForChromeY}});

        } else {
			UIALogger.logMessage("chrome not seen");
	}
	target.delay(3);
	return;
}

/*-----------------------------------------------------------*
 * @ showChromeNoSwipe
 * UI : in reader view 
 * show the chrome, but stay on the page to get the chrome up unless
 *  a web view forces me to move.
 *-----------------------------------------------------------*/
function showChromeNoSwipe() {
	var autoSwipeForward=0;
	showChrome(autoSwipeForward);
}

/*-----------------------------------------------------------*
 * @ showChromeSwipe
 * UI : in reader view 
 * show the chrome, but swipe forward a page if chrome not immediately seen
 *-----------------------------------------------------------*/
function showChromeSwipe() {
	var autoSwipeForward=1;
	showChrome(autoSwipeForward);
}

/*-----------------------------------------------------------*
 * @ back to Lib 
 * UI : in Reader View
 *      Will bring up chrome and bring user back to Lib View
 *      Does not work with BHG landscape
 *-----------------------------------------------------------*/
function tapBackToLib() {
	var libraryButtonTimeout=10;
	var libraryButtonCheck=1;
	target.delay(2);
	showChrome();
	//the first libraryButton is giving us problems, so we are more generic now.
        //libraryButton = UIATarget.localTarget().frontMostApp().navigationBar().buttons()["chrome backbutton up library r"];
        libraryButton = UIATarget.localTarget().frontMostApp().navigationBar().buttons()[0];
	libraryButton.tap();
}

/*-----------------------------------------------------------*
 * swipeForward
 * UI : in Reader View
 *-----------------------------------------------------------*/
function swipeForward() {
	 var orientation = UIATarget.localTarget().frontMostApp().interfaceOrientation()
        UIALogger.logMessage("orientation is "+orientation);
        switch (orientation) {
          case UIA_INTERFACE_ORIENTATION_LANDSCAPELEFT:
          case UIA_INTERFACE_ORIENTATION_LANDSCAPERIGHT:
                UIALogger.logDebug("Landscape");
        	target.dragInsideWithOptions({startOffset:{x:0.3, y:0.9}, endOffset:{x:0.3, y:0.3}, duration:0.5});
                break;
          default:
        	target.dragInsideWithOptions({startOffset:{x:0.9, y:0.3}, endOffset:{x:0.3, y:0.3}, duration:0.5});
        }
} 

function swipeForwardToNextWheel() {

	var orientation = UIATarget.localTarget().frontMostApp().interfaceOrientation()
        UIALogger.logMessage("orientation is "+orientation);
        switch (orientation) {
          case UIA_INTERFACE_ORIENTATION_LANDSCAPELEFT:
          case UIA_INTERFACE_ORIENTATION_LANDSCAPERIGHT:
                UIALogger.logDebug("Landscape");
      		target.dragInsideWithOptions({startOffset:{x:0.9, y:0.9}, endOffset:{x:0.9, y:0.65}, duration:0.3});
                break;
          default:
      		target.dragInsideWithOptions({startOffset:{x:0.9, y:0.9}, endOffset:{x:0.65, y:0.9}, duration:0.3});
        }
} 

function swipeBackwardToNextWheel() {
	var orientation = UIATarget.localTarget().frontMostApp().interfaceOrientation()
        UIALogger.logMessage("orientation is "+orientation);
        switch (orientation) {
          case UIA_INTERFACE_ORIENTATION_LANDSCAPELEFT:
          case UIA_INTERFACE_ORIENTATION_LANDSCAPERIGHT:
                UIALogger.logDebug("Landscape");
        	target.dragInsideWithOptions({startOffset:{x:0.9, y:0.65}, endOffset:{x:0.9, y:0.9}, duration:0.3});
                break;
          default:
        	target.dragInsideWithOptions({startOffset:{x:0.65, y:0.9}, endOffset:{x:0.9, y:0.9}, duration:0.3});
        }
} 

/*-----------------------------------------------------------*
 * swipeBackward
 * UI : in Reader View
 *-----------------------------------------------------------*/
function swipeBackward() {
	var orientation = UIATarget.localTarget().frontMostApp().interfaceOrientation()
        UIALogger.logMessage("orientation is "+orientation);
        switch (orientation) {
          case UIA_INTERFACE_ORIENTATION_LANDSCAPELEFT:
          case UIA_INTERFACE_ORIENTATION_LANDSCAPERIGHT:
                UIALogger.logDebug("Landscape");
        	target.dragInsideWithOptions({startOffset:{x:0.3, y:0.3}, endOffset:{x:0.3, y:0.9}, duration:0.5});
                break;
          default:
        	target.dragInsideWithOptions({startOffset:{x:0.3, y:0.3}, endOffset:{x:0.9, y:0.3}, duration:0.5});
        }
}

/*-----------------------------------------------------------*
 * swipeBackwardInBrowseView
 * UI : in Reader Browse View
 *-----------------------------------------------------------*/
function swipeBackwardInBrowseView() {
	var orientation = UIATarget.localTarget().frontMostApp().interfaceOrientation()
        UIALogger.logMessage("orientation is "+orientation);
        switch (orientation) {
          case UIA_INTERFACE_ORIENTATION_LANDSCAPELEFT:
          case UIA_INTERFACE_ORIENTATION_LANDSCAPERIGHT:
                UIALogger.logDebug("Landscape");
		target.dragInsideWithOptions({startOffset:{x:"0.5", y:"0.4"}, endOffset:{x:"0.5", y:"0.6"}, duration:"0.5"})
                break;
          default:
		target.dragInsideWithOptions({startOffset:{x:"0.4", y:"0.5"}, endOffset:{x:"0.6", y:"0.5"}, duration:"0.5"})
        }
}

/*-----------------------------------------------------------*
 * swipeForwardInBrowseView
 * UI : in Reader Browse View
 *-----------------------------------------------------------*/
function swipeForwardInBrowseView() {
	var orientation = UIATarget.localTarget().frontMostApp().interfaceOrientation()
        UIALogger.logMessage("orientation is "+orientation);
        switch (orientation) {
          case UIA_INTERFACE_ORIENTATION_LANDSCAPELEFT:
          case UIA_INTERFACE_ORIENTATION_LANDSCAPERIGHT:
                UIALogger.logDebug("Landscape");
		target.dragInsideWithOptions({startOffset:{x:"0.5", y:"0.6"}, endOffset:{x:"0.5", y:"0.4"}, duration:"0.5"})
                break;
          default:
		target.dragInsideWithOptions({startOffset:{x:"0.6", y:"0.5"}, endOffset:{x:"0.4", y:"0.5"}, duration:"0.5"})
        }
}

/*-----------------------------------------------------------*
 * doVerifyIndexOverlay
 * UI : in Reader View
 * Purpose: Verifies the Index Overlay matches browse view (currently article title only)
 * Works in:  Midwest Living - Nov/Dec 2012
 *-----------------------------------------------------------*/
function doVerifyIndexOverlay() {
        var maxTriesToCheckInReader = 5;
        var numIsInReaderChecks = 0;
        while (isInReader() == 0 && numIsInReaderChecks < maxTriesToCheckInReader ) {
                numIsInReaderChecks++;
        }
        if (numIsInReaderChecks == maxTriesToCheckInReader) {
                UIALogger.logFail("Can't verify index overlay.  We're not in the reader!");
                return 0;
        }

        getTitleAndIssueFromReader();

        showIndexOverlay();
        var iOWindow=window.popover().tableViews();
        iOWindow[0].cells()[0].scrollToVisible();
        var numArticlesInOverlay=iOWindow[0].cells().length-1;

        for (var i=0; i<=numArticlesInOverlay ; i++) {
                if ( iOWindow[0].cells()[i].name() == null) {
                        UIALogger.logFail("cell null "+i);

                } else {
                        var indexOverlayTitle=iOWindow[0].cells()[i].name();
                        var indexOverlayList=indexOverlayTitle.split(",");
                        if (indexOverlayList.length > 1) {
                                var kicker=trimWhiteSpace(indexOverlayList[0]);
                                UIALogger.logMessage("Kicker: "+kicker);
                                var articleTitle=trimWhiteSpace(indexOverlayList[1]);
                                UIALogger.logMessage("Article: "+articleTitle);
                        } else {
                                var articleTitle=trimWhiteSpace(indexOverlayList[0]);
                                UIALogger.logMessage("Article: "+articleTitle);
                        }


                        iOWindow[0].cells()[i].tap();
                        target.delay(1);

                        var indexMatchesBrowseView = verifyMetaData(articleTitle);
                        if (indexMatchesBrowseView ==1) {
                            UIALogger.logPass("Index Overlay Matches Browse View for "+articleTitle);
                        } else  {
                            UIALogger.logFail("Index Overlay No Match Browse View for "+articleTitle);
                        }
                        target.tap({x:15.00, y:85.00});
                        if (i < numArticlesInOverlay) {
                                showIndexOverlay();
                        }
                }
        }
        target.tap({x:348.00, y:342.00});
        tapBackToLib();
	UIALogger.logPass("doVerifyIndexOverlay OK");
}



/*-----------------------------------------------------------*
 * showIndexOverlay
 * UI : in Reader View
 *-----------------------------------------------------------*/
function showIndexOverlay() {
	UIALogger.logMessage("Show Index Overlay");
        showChrome();
        var indexOverlayButton = UIATarget.localTarget().frontMostApp().navigationBar().buttons()["index up"];
        indexOverlayButton.tap();
}
/*-----------------------------------------------------------*
 * isIndexOverlayButtonVisible
 * UI : in Reader View
 *-----------------------------------------------------------*/
function isIndexOverlayButtonVisible() {
        var indexOverlayButton = UIATarget.localTarget().frontMostApp().navigationBar().buttons()["index up"];
	if (indexOverlayButton.isVisible() == 1) {
		return 1;
	} else {
		return 0;
	}
}

function dismissIndexOverlay() {
	var orientation = UIATarget.localTarget().frontMostApp().interfaceOrientation()
	UIALogger.logMessage("orientation is "+orientation);
	switch (orientation) {
	  case UIA_INTERFACE_ORIENTATION_LANDSCAPELEFT:
	  case UIA_INTERFACE_ORIENTATION_LANDSCAPERIGHT:
		UIALogger.logDebug("Landscape");
		target.tap({x:758.00, y:102.00});
		break;
	  default:
		target.tap({x:660.00, y:994.50});
	}	
}

/*-----------------------------------------------------------*
 * verifyMetaData
 * UI : in Reader View
 * Purpose: Verifies the meta-data for browse view on the current page.
 * Parameter:  articleTitle
 *-----------------------------------------------------------*/
function verifyMetaData(articleTitle) {
	var autoSwipeForward=0;
        showChromeNoSwipe();
        target.delay(1);
        var foundArticleTitle=0;
        var numVerifyMetaDataTries=0;
        var sliderValue=window.sliders()[0].value();
        var sliderValueDec = parseFloat(sliderValue);
	var indexBrowseCount=0;
	var indexBrowseMax=6;
        sliderValueDec /= 100;
        UIALogger.logMessage("slider value "+sliderValueDec);


	//The slider needs to be moved at least ".01" from its current location
	//   to bring up Browse View.
	//In most cases, we will increment the slider value, drag, then swipe backwards if needed.
	//At the end of the issue this doesn't work (can't slide past 1.0) so we reverse the above

        target.delay(1);
        if (sliderValueDec < .98) {
                sliderValueDec += .01;
        	window.sliders()[0].dragToValue(sliderValueDec);
		var swipeBackwardInBrowseIfNeeded=1;
        } else {
                sliderValueDec -= .01;
        	window.sliders()[0].dragToValue(sliderValueDec);
		var swipeBackwardInBrowseIfNeeded=0;
	}
			
	
	//We will either swipe forward or backward in browse view (depending on the above)
	//   If we didn't get a match.
	//We will do this for "indexBrowseMax" amount of times.  Then, if no match, we fail.
        while (matchMetaDataBrowseView(articleTitle) == 0 && indexBrowseCount < indexBrowseMax) {  
		UIALogger.logMessage("here again. swiping backward??");
		if (swipeBackwardInBrowseIfNeeded==1) {
			swipeBackwardInBrowseView();
		} else { 
			swipeForwardInBrowseView();
		}
		indexBrowseCount++;
        }
	if (indexBrowseCount < indexBrowseMax) {
		UIALogger.logMessage("horray");
		return 1;	
	} else {
		UIALogger.logMessage("BOO");
		return 0;
	}


}

/*-----------------------------------------------------------*
 * matchMetaDataBrowseView
 * UI : in Browse (Reader) View
 * Purpose: tests for a match of the articleTitle passed in.
 * Called By: verifyMetaData
 * Returns: 1 (match); 0 (no match)
 *-----------------------------------------------------------*/
function matchMetaDataBrowseView(articleTitle) {
        target.delay(1);
        var browseViewStaticTexts = app.mainWindow().staticTexts();

	var str = browseViewStaticTexts[0].name();
	UIALogger.logMessage("str is "+str+" article title is "+articleTitle);
        if (str != null) {
             //if (str.match("^"+articleTitle+"$") != null) { return 1;
             if (articleTitle == str) {
	 	 return 1;
	     } else {
		return 0;
	     }
	} else {
	     return 0;
	}

}


/*-----------------------------------------------------------*
 * getSelectedEntryFromIndexOverlay
 * UI : in Reader
 * Purpose: returns what is selected in the Index Overlay
 * Returns: null (default); text in selection (if found)
 *-----------------------------------------------------------*/
function getSelectedEntryFromIndexOverlay (){

        showIndexOverlay();
        var maxElementsIndexOverlay = app.mainWindow().popover().tableViews()[0].elements();
        var eleLenIndexOverlay = maxElementsIndexOverlay.length;
        var selectedIndexOverlayPosition = -1;
        var selectedIndexOverlayName="null";

        for (var i=0; i<eleLenIndexOverlay; i++) {
                var currentIndexOverlayElement = app.mainWindow().popover().tableViews()[0].elements()[i];
                //UIALogger.logMessage ("overlay name: "+currentIndexOverlayElement.name());
                var indexOverlaySelected = currentIndexOverlayElement.value();
                if (indexOverlaySelected == "1"){
                        selectedIndexOverlayPosition = i;
                        selectedIndexOverlayElement=app.mainWindow().popover().tableViews()[0].elements()[selectedIndexOverlayPosition];
                        selectedIndexOverlayName=selectedIndexOverlayElement.name();
                }
        }
        return selectedIndexOverlayName;
}

/*-----------------------------------------------------------*
 * @ isThisSelectedInTheIndex
 * UI : in reader view
 * Purpose: Given a string, usually a seciton wheel name, is it selected in the Index Overlay.
 * Returns: 1=yes; 0=no.
 *-----------------------------------------------------------*/
function isThisSelectedInTheIndex (currentWheelName) {
	UIALogger.logMessage("in isThisSelectedInTheIndex");
        var isSelected = 0;
        showIndexOverlay();
	
	
	//For var i =0 i< eleLen i ++
	//If string match on thisIndex.name to currentWheelName
	//Get the position of this index.
	//Make that "this index"

	var IndexLen=app.mainWindow().popover().tableViews()[0].elements().length;

	for (var w=0; w<IndexLen; w++) {
		var eachIndex= app.mainWindow().popover().tableViews()[0].elements()[w];
		var eachIndexName = eachIndex.name();
		if (eachIndexName != null) {
			if (eachIndexName.match(currentWheelName)) {
				thisIndex=eachIndex;
				UIALogger.logMessage("this index name is "+thisIndex.name());
        			isSelected = thisIndex.isEnabled();
        			target.tap({x:348.00, y:342.00});
				target.delay(1);
				return isSelected;
			}
		} else {
		   UIALogger.logWarning("We got a null index for the above wheel name!");
		}
	}
	return 0;
		

	
}


/*-----------------------------------------------------------*
 * @ clickDoneOnWebViewIfExists
 * UI : in a Web View with the "Done" button 
 * Purpose:  Clicks "Done"
 * Will exit gracefully if web view does not exist
 * Returns 1:  was a web view; 0: no web view
 *-----------------------------------------------------------*/
function clickDoneOnWebViewIfExists() {
        var webviewDoneButton = UIATarget.localTarget().frontMostApp().mainWindow().buttons()["Done"];
	var cycle = 0;
	var timeout = 2;
        if (webviewDoneButton.isVisible()) {
	    UIALogger.logMessage("web view is visible.  tapping on it now");
	    target.frontMostApp().mainWindow().buttons()["Done"].tap();
	    return 1;
	} else {	
	    return 0;
	}
}

/*-----------------------------------------------------------*
* @swipeForPageMissing(direction)
* swipes Forward or Backward depending on parameter.
* checks for a Page Missing.
*-----------------------------------------------------------*/
function swipeForPageMissing(direction) {
      var maxPagesSwipe=13;
      var minutes_waiting_for_missing=0;
      var max_minutes_waiting=15;
      var pageMissing;

      for (i=0; i<maxPagesSwipe; i++) {
        target.delay(1);

        if (direction == "forward") {
                swipeForward();
        } else {
                swipeBackward();
        }
      }

     while (pageMissing == null && minutes_waiting_for_missing < max_minutes_waiting ) { 
	     UIALogger.logMessage("min waitin for missing = "+minutes_waiting_for_missing);
	     pageMissing=window.scrollViews()[0].scrollViews()[1].staticTexts()[0].name();
     	     if (pageMissing=="Page Missing") {
          	UIALogger.logWarning("We received a page missing on page swipe # "+i);
     	     } else {
		     UIALogger.logMessage("No page missings yet.  Waiting another minute");
		     minutes_waiting_for_missing++;
		     target.delay(60);	
	      }
     }
	UIALogger.logMessage("exited while loop");	
}

/*-----------------------------------------------------------*
* @getTitleAndIssueFromReader()
* Purpose:  using the index overlay, Prints the Title and Issue currently in
* UI:  Reader view
*-----------------------------------------------------------*/
function getTitleAndIssueFromReader() {
   showIndexOverlay();
   var iOStaticTexts=window.popover().navigationBar().staticTexts();
   var numStaticTexts=iOStaticTexts.length;
   var title=iOStaticTexts[0].name();
   var issue=iOStaticTexts[1].name();
   UIALogger.logPass("Title is "+title+". Issue is "+issue+".");
   dismissIndexOverlay();
   target.delay(1);
   //window.scrollViews()[0].scrollViews()[0].tapWithOptions({tapOffset:{x:offsetForChromeX, y:offsetForChromeY}});
   target.tap({x:348.00, y:342.00});
   UIALogger.logPass("Success in getting title and issue");
}


 /*-----------------------------------------------------------*
 * @ createFinalWheelList
 * UI : in reader view
 * Purpose:  Given the start of an  issue, inventory every single page and create sequential, unique wheel entries
 *-----------------------------------------------------------*/
function createFinalWheelList() {
	//Initialize chrome slider to 0 to start section wheel inventory at the beginning
        showChromeNoSwipe();
	var sliderValueForWheelComp=0;
        window.sliders()[0].dragToValue(sliderValueForWheelComp);
	var finalWheelList = [];

	//While the slider has not reached the end of the issue...
	 while (sliderValueForWheelComp  < 1) {
	  //sliderValueWheelReturnPos = sliderValueForWheelComp;

	  //Initialize "current" static texts in memory
          var wheelStaticTexts = target.frontMostApp().mainWindow().staticTexts();
	  var wheelStaticTextsLength= target.frontMostApp().mainWindow().staticTexts().length;
	  var bWheelInList=0;

	  //Go through this current list of static texts in memory...
	  for (var r = 3; r < wheelStaticTextsLength; r++) {
	 	 var wheelStaticTextName=wheelStaticTexts[r].name();
  
	  	//If a static text is in the final wheel list, don't add it again and don't tap on it
	    	  for (var t = 0; t < finalWheelList.length; t++) {
		        if (finalWheelList[t] == wheelStaticTextName) {
       		    	    var bWheelInList=1;
			}
		   }

	  //If static text is NOT in the final wheel list, add it. 
		  if (bWheelInList == 0) {
		  	finalWheelList.push(wheelStaticTextName);
		  }

       	  }

	  //Inch up the slider..We will start the while loop again to see if the static texts have changed
	  while (UIATarget.localTarget().frontMostApp().navigationBar().buttons()["index up"].isVisible() == 0) {
		swipeBackward();	
	        showChrome();
	  }
	  sliderValueForWheelComp +=.01;
          window.sliders()[0].dragToValue(sliderValueForWheelComp);

	}

	//Add the last few "current" wheel items to our "full wheel list"
	for (var r=4; r < wheelStaticTextsLength; r++) {
		var wheelStaticTextName=wheelStaticTexts[r].name();	
		finalWheelList.push(wheelStaticTextName);
	}
	return finalWheelList;
}

/*--------------------------------------------
 * @ tapOnWheelIfVisible
 * UI:  Reader Content View
 * Purpose:  taps on passed in Wheel Name.  then calls isThisSelectedInTheIndex
 *-------------------------------------------*/
function tapOnWheelIfVisible(theFinalWheelListName) {
	        wheelStaticTexts = target.frontMostApp().mainWindow().staticTexts();
		if (wheelStaticTexts[theFinalWheelListName].isVisible()) {
		    UIALogger.logMessage("list position is enabled.  going to tap it");
		    swipeForwardToNextWheel(); //Used to verify the wheel is indeed visible

	 	    //Known to work except for at edges
		    UIATarget.localTarget().touchAndHold(wheelStaticTexts[theFinalWheelListName].rect(),1);

		    //Expermienting
		    //UIATarget.localTarget().wheelStaticTexts[theFinalWheelListName].tapWithOptions({tapOffSet:{x:0.3, y:0.6}});
		    target.delay(2);

                    var isSelected = isThisSelectedInTheIndex(theFinalWheelListName);
                    UIALogger.logMessage("isSelected: "+isSelected);
                    if (isSelected == 1) {
                        UIALogger.logPass ("Wheel Matches Index Overlay for "+theFinalWheelListName+"!");
                    } else {
                        UIALogger.logFail ("Wheel DOES NOT Match Index Overlay for "+theFinalWheelListName+"!");
                    }
		    //take below line out if causes problems
		    dismissIndexOverlay();
		    return 1;
		} else {
		    return 0;
		}
}

 /*-----------------------------------------------------------*
 * @ doVerifyWheelMatchesIndex
 * UI : in reader view
 * Purpose:  Given an issue, go through the section wheels and match the Index Overlay selection
 *-----------------------------------------------------------*/
function doVerifyWheelMatchesIndex() {
        getTitleAndIssueFromReader();
	showChrome();
	var finalWheelList = createFinalWheelList();

	//Print out our full wheel list
	//Slide to the beginning, offset slider to bring up content view, then tap on the wheel elements one by one
	while (UIATarget.localTarget().frontMostApp().navigationBar().buttons()["index up"].isVisible() == 0) {
		swipeBackward();	
	        showChrome();
	}

	//Slide back to beginning of issue and launch content view
	sliderValueForWheelComp=0;
        window.sliders()[0].dragToValue(sliderValueForWheelComp);
	target.delay(1);
	launchContentFromChrome();
	target.delay(1);

	//For a given entry in the wheel, tap on it (call tapOnWheelIfVisible)
	// this will go verify wheel = index overlay.
	var chromeTryCount = 0;
	var chromeTryMax = 5;
	for (var u=0; u<finalWheelList.length; u++) {
		var chromeTryCount = 0;
		var theFinalWheelListName=finalWheelList[u];
		UIALogger.logMessage("final list position "+u+" is "+theFinalWheelListName);
		var tapWheelSuccess = tapOnWheelIfVisible(theFinalWheelListName);
		while (tapWheelSuccess == 0 && chromeTryCount<=chromeTryMax) {
			showChrome();
			tapWheelSuccess = tapOnWheelIfVisible(theFinalWheelListName);
			chromeTryCount++;
		}
	}
	UIALogger.logPass("doVerifyWheelMatchesIndex pass!");
        tapBackToLib();
}



 /*-----------------------------------------------------------*
 * @ launchChrome
 * UI : in reader contentView
 *--------------------------------------------------------------*/
function launchChrome() {
	showChromeNoSwipe();
	var sliderValue=window.sliders()[0].value();
  	var sliderValueDec = parseFloat(sliderValue);
        sliderValueDec /= 100;
	UIALogger.logMessage("slider value "+sliderValueDec);

	target.delay(1);
        if (sliderValueDec < .98) {
           sliderValueDec += .01;
           window.sliders()[0].dragToValue(sliderValueDec);
         } else {
           sliderValueDec -= .01;
           window.sliders()[0].dragToValue(sliderValueDec);
  	 }
}

 /*-----------------------------------------------------------*
 * @ launchContentFromChrome
 * UI : in reader browse view
 *--------------------------------------------------------------*/
function launchContentFromChrome() {
        var orientation = UIATarget.localTarget().frontMostApp().interfaceOrientation()
        UIALogger.logMessage("orientation is "+orientation);
        switch (orientation) {
          case UIA_INTERFACE_ORIENTATION_LANDSCAPELEFT:
          case UIA_INTERFACE_ORIENTATION_LANDSCAPERIGHT:
                UIALogger.logDebug("Landscape");
                target.dragInsideWithOptions({startOffset:{x:0.9, y:0.3}, endOffset:{x:0.3, y:0.3}, duration:0.5});
                break;
          default:
		//target.frontMostApp().mainWindow().scrollViews()[0].scrollViews()[2].tapWithOptions({tapOffset:{x:0.51, y:0.35}});
		target.frontMostApp().mainWindow().scrollViews()[0].scrollViews()[0].tapWithOptions({tapOffset:{x:0.51, y:0.35}});
		//target.tap({x:348.00, y:342.00});
        }
}

