#import "globals.js"
#import "libfunc.js"
#import "loginfunc.js"
#import "settingfunc.js"
#import "flatlibfunc.js"
#import "readerfunc.js"
#import "downloadfunc.js"
#import "pickerfunc.js"
#import "testcases.js"
#import "utility.js"



//--- APP VERSION define ---
var app_version="howitzer";
var ipa_version="debug";
UIALogger.logMessage("app veresion is "+app_version);

//Used for changing scroll views with Grenade
switch (app_version) {
   case "grenade":
        libScrollViewPosition=1;
        break;
   case "howitzer":
        libScrollViewPosition=0;
	sortNavBarButtonPos=0;
	break;
   default:
        libScrollViewPosition=0;
	sortNavBarButtonPos=0;
}
UIALogger.logMessage("sortNavBarButtonPos is "+sortNavBarButtonPos);

// --- NIM/Rogers User define ---
var app_name="nim"
switch (app_name) {
   case "rogers":
        var username="asdf@nim.com";
        var password="Password2";
        break;
   default:
        var username="foo702@foo.com";
        var password="nimqa123";
}

//---FB Define ----
var FBusername="nimtaco@gmail.com";
var FBpassword="nimqa123";

//which qa, prod environment
var env=qa2;

//self-referencing, used for some of the utility.js functions
var master_js_filename="regression.js"; 




var rtcFBMaster=0;
var rtcLogin1=0;
var rtcCheckAutoRefresh = 0;
var rtcOptIn1 = 0;
var rtcOptIn1Param= "Allure";
var rtcDelete1 = 0;
var rtcDelete2 = 0;
var rtcUsage1 = 0;
var rtcUsage2 = 0;
var rtcUsage3 = 0;
var rtcUsage4 = 0;
var rtcUsage5 = 0;
var rtcSettings1 = 0;
var rtcPD1=0;
var rtcPin1=0;
var rtcVerifyIndexOverlay=0;
var rtcVerifyIndexOverlays=0;

//full regression
var rtcCheckPageMissings= 0;
var rtcCheckPageMissingsWholeLib= 0;
var rtcLibStress1 = 0;
var rtcLibStress2 = 0;




/*-------------------------------------*
*Test Cases that will run based on the  "TC define" section above
*-------------------------------------*/


if (rtcFBMaster == 1) {
    tcFBMaster(FBusername,FBpassword);
}

if (rtcLogin1 ==1){
        tcLogin1(username, password);
}

//Purchase entitlements in store and verify show up in lib
if (rtcCheckAutoRefresh == 1){
        tcCheckAutoRefresh();
}


// Opt In
if (rtcOptIn1 == 1){
        tcOptIn1(rtcOptIn1Param);
}

//if (rtcOptOut1 == 1){
//        tcOptOut1(rtcOptIn2Param);
//}

if (rtcDelete1 == 1) {
        tcDelete1();
}
if (rtcDelete2 == 1) {
        tcDelete2();
}

if (rtcUsage1 == 1){
        tcUsage1();
}
// Test Usage 2: checking chaning max issue works
if (rtcUsage2 ==1){
        tcUsage2();
}

if (rtcUsage3 ==1){
        tcUsage3();
}

if (rtcUsage4 ==1){
        tcUsage4();
}
if (rtcUsage5 ==1){
        tcUsage5();
}
if (rtcSettings1 ==1){
        tcSettings1();
}


if (rtcPD1 == 1){
        tcPD1()
}

if (rtcPin1 == 1){
        tcPin1();
}

if (rtcVerifyIndexOverlay == 1) {
        tcVerifyIndexOverlay();
}


if (rtcVerifyIndexOverlays == 1) {
        tcVerifyIndexOverlays();
}


if (rtcCheckPageMissings == 1){
        tcCheckPageMissings();
}

if (rtcCheckPageMissingsWholeLib == 1){
        tcCheckPageMissingsWholeLib();
}


if (rtcLibStress1 == 1){
        tcLibStress1();
}

if (rtcLibStress2 == 1){
        tcLibStress2();
}

//window.logElementTree();




//Generate issue_open issue close metrics
for (var j=1; j<10; j++) {
for (var p=1; p<10; p++) {
   var issuesIn=buildTitleList();

//Generate issue_open issue close metrics
  openSpecificIssue(p);
//Check ISSUE_OPEN and ISSUE_CLOSE metric

}
}
UIALogger.logMessage("we got through it!");
