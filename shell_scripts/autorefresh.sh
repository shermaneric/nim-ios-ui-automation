#!/bin/bash
max_iterations=1
current_iteration=1
IPAD="ipad2"

#Get the user count from the js and increment by 1
JS_LOGIN_FILENAME=AD_HOC_login.js
JS_AUTOREFRESH_FILENAME=AD_HOC_autorefresh.js
php_dir=/Users/nextissue/Documents/svn_frances_scripts.d/useraccount

JSDIR=/Users/nextissue/Documents/svn_qa_ios.d/ios/js
user_count=`grep 'var username' $JSDIR/$JS_LOGIN_FILENAME | cut -d\" -f2 | cut -d\@ -f1 | egrep "[0-9]{1,}" -o`
let user_count=user_count+1

user=ess$user_count

#put the new email address in the js files.
sed 's/username=\".*$/username=\"'$user'\@test\.com\"/g' $JSDIR/$JS_AUTOREFRESH_FILENAME > $JSDIR/foo.js
mv $JSDIR/foo.js  $JSDIR/$JS_AUTOREFRESH_FILENAME


sed 's/username=\".*$/username=\"'$user'\@test\.com\"/g' $JSDIR/$JS_LOGIN_FILENAME > $JSDIR/foo.js
mv $JSDIR/foo.js  $JSDIR/$JS_LOGIN_FILENAME

#Create a new non-plan user and add 1 sub (Time)
php ./NewUser.php --server qa --domain test --user $user --index -1
php ./AddSub.php --server qa --domain test --user $user --titles "Time"

#while this is runing, open up a new terminal and Add a new single plan 
osascript -e 'tell application "Terminal" to do script "/Users/nextissue/Documents/svn_frances_scripts.d/useraccount/autorefresh_add_ent.sh"'
#run the UI automation to check "X" amount of minutes for refresh.
iosauto.sh $JS_AUTOREFRESH_FILENAME $IPAD

