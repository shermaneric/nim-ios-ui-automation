#!/bin/bash


function FreshInstallUsage {
     echo -e "Usage: $(basename $0) <trunk|branch name>"
     echo -e "\tExample: $(basename $0) excalibur"
     exit 0
}

function FreshInstallIpa {
    #put in iosauto.sh.  Works up to builds that are universal environment ones.
    IPA_MACHINE=apollo.build.nim.com
    IPA_ROOT_DIR=/var/www/releases

    #If branch or trunk not passed, usage.
    BRANCH_OR_TRUNK=$1
    if test "$BRANCH_OR_TRUNK" = ""
    then
      FreshInstallUsage
    fi

    #If a branch name is passed in, prepend the branches directory
    case $BRANCH_OR_TRUNK in
        "trunk")
        JENKINS_LOCATION="$BRANCH_OR_TRUNK"
        ;;
        *)
        JENKINS_LOCATION="branches/$BRANCH_OR_TRUNK"
    esac 

    #get the latest build number.  the latest build number in ipa is for older builds.
    LATEST_BUILD_NUMBER=`ssh esherman@apollo.build.nim.com ls -t /var/www/releases/${JENKINS_LOCATION}/Reader-iOS/promotion/QA | head -1 `
    LATEST_BUILD_NUMBER_IN_IPA=`echo $LATEST_BUILD_NUMBER | cut -d\- -f1`

    #If we cannot find the latest build number, exit
    if [ -z $LATEST_BUILD_NUMBER ]
    then 
	echo "Build not found in Jenkins" 
        FreshInstallUsage
    fi

    #Assign a local directory for the IPA to be copied to
    IPA_LOCAL_DIR="/Users/esherman/Downloads"

    #Find the Jenkins path and ipa to do the copy.  If it's not there, report it and quit
    IPA_JENKINS="/var/www/releases/${JENKINS_LOCATION}/Reader-iOS/promotion/QA/${LATEST_BUILD_NUMBER}/Debug/NextIssue-Debug-${LATEST_BUILD_NUMBER_IN_IPA}.ipa"


    echo "Copying $IPA_JENKINS from Jenkins"
    scp esherman@apollo.build.nim.com:$IPA_JENKINS $IPA_LOCAL_DIR

    #Grab the ipa name (less the jenkins path), find it locally.
    IPA_NAME="${IPA_JENKINS##*/}"

    #Remove old ipa and install the new one
    if test "$IPA_NAME" != ""
    then
        full_local_ipa_path="$IPA_LOCAL_DIR/$IPA_NAME"
        ideviceinstaller -u com.nextissuemedia.Next-Issue
        ideviceinstaller -i $full_local_ipa_path
    fi
}
