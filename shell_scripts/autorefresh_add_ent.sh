#!/bin/sh
#Called from autorefresh.sh to use in concert with the UI automation 

function Usage {
     echo "Usage: $(basename $0) <ent> <title> <env>"
     echo "\tent = sub, issue, basic, premium"
     echo "\tenv = qa,qa2,qa3"
     exit 0
}

JSDIR=/Users/esherman/Documents/svn_qa_ios.d/ios/js
JS_FILENAME=regression.js

my_user=`grep 'var username' $JSDIR/$JS_FILENAME | cut -d\" -f2 | cut -d\@ -f1`
my_domain=`grep 'var username' $JSDIR/$JS_FILENAME | cut -d\" -f2 | cut -d\@ -f2 | cut -d\. -f1`

title=$2
env=$3
case "$1" in
   sub)
	#We add a new subscription...
	php /Users/esherman/Documents/svn_frances_scripts.d/useraccount/AddSub.php --server $env --domain $my_domain --user $my_user  --titles "${title}"

	;;
   issue)
	#We add a new issue
	php /Users/esherman/Documents/svn_frances_scripts.d/useraccount/BuyIssue.php --server $env --domain $my_domain --user $my_user --titles "${title}"
	;;
   basic)
	#We add basic
	php /Users/esherman/Documents/svn_frances_scripts.d/useraccount/AddPlan.php --server $env --domain $my_domain --user $my_user --plan B
	;;
   premium)
	#We add premium
	php /Users/esherman/Documents/svn_frances_scripts.d/useraccount/AddPlan.php --server $env --domain $my_domain --user $my_user --plan P
	;;
    *)
        Usage
	exit 0
esac

