#!/bin/bash
#-----------------------------
#Name: iosauto.sh
#Author: Eric Sherman
#Purpose: Uses xcode instruments to run desired automation script against the NIM app
#Usage: iosauto.sh <script> <device>
#Example:   iosauto.sh smoketest.js ipad1
#	Runs smoketest.js (located in SCRIPT_PATH)
#       using the UDID in the IPAD1 variable below
#-----------------------------

source ~/scripts_utils/ipad_devices_simulators.sh

SCRIPT=$1
device=$2
myIpa=$3

function Usage { 
     echo "Usage: $(basename $0) script device ipa (optional)"
     echo -e "\tscript = any .js script you wish to use for automation"
     echo -e "\tdevice = ipad1, ipad2, ipad3, ipad4, ipadmini, simulator"
     echo -e "\tipa = name of ipa.  Current path is $full_ipa_path"
     exit 0
}

KillPids () {
for i in `ps -ef | grep 'iosauto.sh' | egrep -v 'grep|vi' | grep -v $$ | awk '{print $2}'`
do
     echo "killing process $i"
     kill -9 $i
done
}


#Function to check if the filename user gives already exists.  If so, exit the program.
if [ ! -f "$SCRIPT_PATH/$SCRIPT" ]
then
  echo "$SCRIPT not found in $SCRIPT_PATH"
  Usage
fi

#Function to check if an ipa was passed in.  Then, we assume the user wants a fresh install
if test "$myIpa" != ""
then
	full_ipa_path="$ipa_dir/$myIpa"
	ideviceinstaller -u com.nextissuemedia.Next-Issue
	ideviceinstaller -i $full_ipa_path
fi

KillPids
##########
#Check the device and start the correct instrument
##########
case "$device" in
  ipad1)
	IPAD=$IPAD1
	#sudo xcode-select -switch $xcode_app_path
	instruments -w $IPAD -t $auto_tmpl_file "Next Issue"  -e UIASCRIPT $SCRIPT_PATH/$SCRIPT -e UIARESULTSPATH $RESULTS_DIR
	;;
  ipad2)
	IPAD=$IPAD2
	#sudo xcode-select -switch $xcode_app_path
	instruments -w $IPAD -t $auto_tmpl_file "Next Issue"  -e UIASCRIPT $SCRIPT_PATH/$SCRIPT -e UIARESULTSPATH $RESULTS_DIR
	;;
  ipad3)
	IPAD=$IPAD3
	#sudo xcode-select -switch $xcode_app_path
	instruments -w $IPAD -t $auto_tmpl_file "Next Issue"  -e UIASCRIPT $SCRIPT_PATH/$SCRIPT -e UIARESULTSPATH $RESULTS_DIR
	;;

  ipad4)
	IPAD=$IPAD4
	#sudo xcode-select -switch $xcode_app_path
	instruments -w $IPAD -t $auto_tmpl_file "Next Issue"  -e UIASCRIPT $SCRIPT_PATH/$SCRIPT -e UIARESULTSPATH $RESULTS_DIR
	;;

  ipadmini)
	IPAD=$IPADMINI
	#sudo xcode-select -switch $xcode_app_path
	instruments -w $IPAD -t $auto_tmpl_file "Next Issue"  -e UIASCRIPT $SCRIPT_PATH/$SCRIPT -e UIARESULTSPATH $RESULTS_DIR 
	;;

  simulator)
	instruments -t "${auto_tmpl_file}" "${APP_FULL_PATH}" -e UIASCRIPT $SCRIPT_PATH/$SCRIPT -e UIARESULTSPATH $RESULTS_DIR
	;;
  *)
       Usage
	;;
esac

