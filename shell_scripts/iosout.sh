#!/bin/bash
#-----------------------------
#Name: iosout.sh
#Author: Eric Sherman
#Purpose:  Takes the iOS Automation .plist results and converts into a csv
#Examples:  iosout.sh passfail "Smoke Test"
#
#Assumption:  web_publish_dir assumes 'mounts' has been run
   If giving you problems, you can ignore.  Output will still go to your local web server
#-----------------------------


csv_filename=nimios_testresults.csv
web_publish_dir=/private/nfs/QA/esherman/for_ken
date=$(date  +%Y_%m_%d_%H:%M:%S)
function Usage {
     echo "Usage: $(basename $0) log_level build_vers"
     echo "log_level Choices:  error|fail|passfail|default|debug"
     echo "build_vers:  description of which build we are testing"
     exit 0
}

> $csv_filename

case $1 in
error)
   log_level="Start|Error"
   ;;
fail)
   log_level="Start|Error|Fail" 
   ;;
passfail)
   log_level="Start|Error|Pass|Fail|Warning"
   ;;
default)
   log_level="Start|Error|Pass|Fail|Warning|Default"
   ;;
debug)
   log_level="Start|Error|Pass|Fail|Warning|Default|Debug"
   ;;
*)
  Usage
  exit 0
esac

if test "$2" == "" ; then 
   Usage
fi

cat "Automation Results.plist" | grep string | cut -d\< -f2 | cut -d\> -f2 > tmp1

echo "NIM iOS Results: " >> tmp2
echo "$2" >> tmp2
echo "$date" >> tmp2
echo "\n\n" >> tmp2
echo "Status,,,,,Test Step" >> tmp2
echo "=====,,,,,==========" >> tmp2

while read line
do
case "$line" in
   Start|Pass|Fail|Debug|Default|Warning|Error)
	echo "$line,,,,,"  | tr -d '\n' >> tmp2
    ;;
   *,*)
	echo "$line" | sed -e 's/,/ /' >> tmp2
    ;;
   *)
    echo "$line">> tmp2
    continue
esac

done < tmp1


cat tmp2 | egrep "$2|$log_level|Status|\==|NIM|$1|$date" | grep -v "/UIAutomation-271/Framework/UIAElement.m" > $csv_filename

chmod 777 $csv_filename

rm tmp1 tmp2

iospost.sh
echo "Script finished!!"
