#!/bin/sh
#$1 = stop (stops shaping)
#  OR start (starts shaping)

case $1 in
start)
  ssh esherman@10.115.51.72 "./start_shaping.sh"
  ;;
stop)
  ssh esherman@10.115.51.72 "./stop_shaping.sh"
  ;;
  *)
   echo "Usage"  
  ;;
esac

