#!/bin/bash
#-----------------------------
#Name: iosauto.sh
#Author: Eric Sherman
#Purpose: Uses xcode instruments to run desired automation script against the NIM app
#Usage: iosauto.sh <script> <device>
#Example:   iosauto.sh smoketest.js ipad1
#	Runs smoketest.js (located in SCRIPT_PATH)
#       using the UDID in the IPAD1 variable below
#-----------------------------
source ~/scripts_utils/ipad_devices_simulators.sh

SCRIPT=$1
USER_PREFIX=iossmoke
device=$2
ENV=$3
TITLES=$4
myIpa=$5
NOPLAN=$6
function Usage { 
     echo "Usage: $(basename $0) <script> <device> <env> <title> <ipa> <noplan>"
     echo -e "\tscript = any .js script you wish to use for automation"
     echo -e "\tdevice = ipad1, ipad2, ipad3, ipad4, ipadmini, simulator"
     echo -e "\tenv = qa, qa2, qa3"
     echo -e "\ttitle (optional) = \"Better Homes and Gardens\",\"The New Yorker\"."
     echo -e "\t\tYou can list as many titles as you like using the above format."
     echo -e "\t\tIf no title given, a new account with 0 opt-ins will be created."
     echo -e "\tipa = The ipa we want to use for a Fresh install."  
     echo -e "\tnoplan (optional).  No argument = plan user; If noplan = \"noplan\", then create a non-unlimited account."
     exit 0
}

##########
#Check the environemnt...
##########
case "$ENV" in 
  qa) 
	ENV="QA"
	;;
  qa2) 
	ENV="QA2"
	;;
  qa3) 
	ENV="QA3"
	;;
    *)
	Usage
	;;
esac

#Create A new User and put that in the file
user_count=`grep 'var username' $SCRIPT_PATH/$SCRIPT | cut -d\" -f2 | cut -d\@ -f1 | egrep "[0-9]{1,}" -o` 

echo $user_count
let user_count=user_count+1
user=${USER_PREFIX}${user_count}



php ./NewUser.php --server $ENV --domain test --user $user --index -1
php ./AddSub.php --server $ENV --domain test --user $user --titles "${TITLES}"
if [ "$NOPLAN" != "noplan" ]
then
	php ./AddPlan.php --server $ENV --domain test --user $user  --plan P
fi

#put the new email address in the smoketest.js and regression.js files.

sed -e 's/username=\".*$/username=\"'$user'\@test\.com\"/g' -e 's/env=.*$/env='$3'/g' $SCRIPT_PATH/smoketest.js > $SCRIPT_PATH/foo.js
mv $SCRIPT_PATH/foo.js  $SCRIPT_PATH/smoketest.js

sed -e 's/username=\".*$/username=\"'$user'\@test\.com\"/g' -e 's/env=.*$/env='$3'/g' $SCRIPT_PATH/regression.js > $SCRIPT_PATH/foo.js
mv $SCRIPT_PATH/foo.js  $SCRIPT_PATH/regression.js

#Function to check if an ipa was passed in.  Then, we assume the user wants a fresh install
if test "$myIpa" != ""
then
        full_ipa_path="$ipa_dir/$myIpa"
        ideviceinstaller -u com.nextissuemedia.Next-Issue
        ideviceinstaller -i $full_ipa_path
fi

#Kill existing instrument PIDs to clean things up
KillPids () {
for i in `ps -ef | grep 'iosauto.sh' | egrep -v 'grep|vi' | grep -v $$ | awk '{print $2}'`
do
     echo "killing process $i"
     kill -9 $i
done
}


#Function to check if the filename user gives already exists.  If so, exit the program.
if [ ! -f "$SCRIPT_PATH/$SCRIPT" ]
then
  echo "$SCRIPT not found in $SCRIPT_PATH"
  Usage
fi

##########
#Check the device and start the correct instrument
##########
case "$device" in
  ipad1)
	IPAD=$IPAD1
	instruments -w $IPAD -t $auto_tmpl_file "Next Issue"  -e UIASCRIPT $SCRIPT_PATH/$SCRIPT -e UIARESULTSPATH $RESULTS_DIR
	;;
  ipad2)
	IPAD=$IPAD2
	instruments -w $IPAD -t $auto_tmpl_file "Next Issue"  -e UIASCRIPT $SCRIPT_PATH/$SCRIPT -e UIARESULTSPATH $RESULTS_DIR
	;;
  ipad3)
	IPAD=$IPAD3
	instruments -w $IPAD -t $auto_tmpl_file "Next Issue"  -e UIASCRIPT $SCRIPT_PATH/$SCRIPT -e UIARESULTSPATH $RESULTS_DIR
	;;

  ipad4)
	IPAD=$IPAD4
	instruments -w $IPAD -t $auto_tmpl_file "Next Issue"  -e UIASCRIPT $SCRIPT_PATH/$SCRIPT -e UIARESULTSPATH $RESULTS_DIR
	;;

  ipadmini)
	IPAD=$IPADMINI
	instruments -w $IPAD -t $auto_tmpl_file "Next Issue"  -e UIASCRIPT $SCRIPT_PATH/$SCRIPT -e UIARESULTSPATH $RESULTS_DIR
	;;

  simulator)
	instruments -t "${auto_tmpl_file}" "${APP_FULL_PATH}" -e UIASCRIPT $SCRIPT_PATH/$SCRIPT -e UIARESULTSPATH $RESULTS_DIR
	;;
  *)
       Usage
	;;
esac


