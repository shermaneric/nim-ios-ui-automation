#!/bin/bash
max_iterations=2
current_iteration=1
IPAD="ipad4"

#Get the user count from the js and increment by 1
JS_FILENAME=loginlogout.js
JSDIR=/Users/nextissue/Documents/svn_qa_ios.d/ios/js
user_count=`grep 'var username' $JSDIR/$JS_FILENAME | cut -d\" -f2 | cut -d\@ -f1 | egrep "[0-9]{1,}" -o`
let user_count=user_count+1




while [ $current_iteration -le $max_iterations ]
do
  echo $user
  user=ess$user_count
  let user_count=user_count+1
  let current_iteration=current_iteration+1

  #put the new email address in the js.
  sed 's/username=\".*$/username=\"'$user'\@test\.com\"/g' $JSDIR/$JS_FILENAME > $JSDIR/foo.js
  mv $JSDIR/foo.js  $JSDIR/$JS_FILENAME


#Create a new non-plan user
php ./NewUser.php --server qa --domain test --user $user --index -1
php ./AddSub.php --server qa --domain test --user $user --titles "Golf","Self","Time"
php ./GetInfo.php --server qa --domain test --user $user --info M,H,O

#run the UI automation to sign in and sign out
iosauto.sh $JS_FILENAME $IPAD

#Upgrade to basic
php ./AddPlan.php --server qa --domain test --user $user --plan B
php ./GetInfo.php --server qa --domain test --user $user --info M,H,O

#run the UI automation to sign in and sign out
iosauto.sh $JS_FILENAME $IPAD

done


